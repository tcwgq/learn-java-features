package com.tcwgq.dom;

import org.junit.Test;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

/**
 * JDK自带DOM解析
 *
 * @author lenovo
 */
public class DomTest {
    /**
     * 查询所有节点
     *
     * @throws Exception
     */
    @Test
    public void selectAll() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File("src/main/resources/people.xml");
        Document doc = db.parse(file);
        NodeList nl = doc.getElementsByTagName("name");
        for (int i = 0; i < nl.getLength(); i++) {
            Node node = nl.item(i);
            // String getTextContent()
            System.out.println(node.getTextContent());
        }
    }

    /**
     * 查询某一节点
     *
     * @throws Exception
     */
    @Test
    public void selectOne() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File("src/main/resources/people.xml");
        Document doc = db.parse(file);
        NodeList nl = doc.getElementsByTagName("name");
        Node node = nl.item(0);
        System.out.println(node.getTextContent());
    }

    /**
     * 添加节点
     *
     * @throws Exception
     */
    @Test
    public void add() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File("src/main/resources/people.xml");
        Document doc = db.parse(file);
        Node people = doc.getElementsByTagName("people").item(0);
        Element person = doc.createElement("person");
        Element name = doc.createElement("name");
        Text nameText = doc.createTextNode("tcwgq");
        name.appendChild(nameText);
        Element age = doc.createElement("age");
        Text ageText = doc.createTextNode("27");
        age.appendChild(ageText);
        person.appendChild(name);
        person.appendChild(age);
        people.appendChild(person);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.transform(new DOMSource(doc),
                new StreamResult(new File("people.xml")));
    }

    /**
     * 修改节点 下面这种方法虽然能实现对节点的修改，但是对XML格式有所要求
     *
     * @throws Exception
     */
    @Test
    public void edit() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File("src/main/resources/people.xml");
        Document doc = db.parse(file);
        Node node = doc.getElementsByTagName("person").item(0);
        NodeList list = node.getChildNodes();
        Node name = list.item(0);
        name.setTextContent("张三");
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.transform(new DOMSource(doc),
                new StreamResult(new File("people.xml")));
    }

    /**
     * 删除节点
     *
     * @throws Exception
     */
    @Test
    public void delete() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File("src/main/resources/people.xml");
        Document doc = db.parse(file);
        Element root = doc.getDocumentElement();
        Node node = doc.getElementsByTagName("person").item(0);
        root.removeChild(node);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer();
        t.transform(new DOMSource(doc),
                new StreamResult(new File("people.xml")));
    }

    /**
     * 遍历节点
     *
     * @throws Exception
     */
    @Test
    public void traverse() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        File file = new File("src/main/resources/people.xml");
        Document document = db.parse(file);
        traverse(document);
    }

    private static void traverse(Node node) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            System.out.println(node.getNodeName());
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node childNode = list.item(i);
            traverse(childNode);
        }
    }

}
