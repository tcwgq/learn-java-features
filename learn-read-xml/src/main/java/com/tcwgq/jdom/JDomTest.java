package com.tcwgq.jdom;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class JDomTest {
    /**
     * 遍历节点
     *
     * @throws IOException
     * @throws JDOMException
     */
    @Test
    public void read() throws IOException, JDOMException {
        // 1.创建一个SAXBuilder的对象
        SAXBuilder saxBuilder = new SAXBuilder();
        // 2.创建一个输入流，将xml文件加载到输入流中
        InputStream in = new FileInputStream("src/main/resources/people.xml");
        InputStreamReader isr = new InputStreamReader(in, StandardCharsets.UTF_8);
        // 3.通过saxBuilder的build方法，将输入流加载到saxBuilder中
        Document document = saxBuilder.build(isr);
        // 4.通过document对象获取xml文件的根节点
        Element rootElement = document.getRootElement();
        // 5.获取根节点下的子节点的List集合
        List<Element> bookList = rootElement.getChildren();
    }
}
