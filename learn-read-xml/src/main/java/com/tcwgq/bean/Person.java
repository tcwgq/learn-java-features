package com.tcwgq.bean;

import lombok.Data;

@Data
public class Person {
    private String id;
    private String name;
    private Integer age;
}
