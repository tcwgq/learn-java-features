package com.tcwgq.bean;

import lombok.Data;

@Data
public class Student {
    private Integer id;
    private String name;
    private Integer age;
}
