package com.tcwgq.utils;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.List;

public class Dom4jUtilsTest {
    public static void main(String[] args) {
        // add();
        // removeLast();
        // removeById("111");
        // selectStudentById("111");
    }

    public static void add() {
        Document document = Dom4jUtils.getDocument("student.xml");
        Element root = document.getRootElement();
        Element student = DocumentHelper.createElement("student");
        Element id = DocumentHelper.createElement("id");
        id.setText("123");
        Element name = DocumentHelper.createElement("name");
        name.setText("aaa");
        Element age = DocumentHelper.createElement("age");
        age.setText("123");
        student.add(id);
        student.add(name);
        student.add(age);
        root.add(student);
        Dom4jUtils.write("student.xml", document);
    }

    private static void removeLast() {
        Document document = Dom4jUtils.getDocument("student.xml");
        Element root = document.getRootElement();
        Node student = document.selectSingleNode("/class/student[last()]");
        root.remove(student);
        Dom4jUtils.write("student.xml", document);
    }

    // 根据ID删除元素。
    private static void removeById(String id) {
        Document document = Dom4jUtils.getDocument("student.xml");
        Element root = document.getRootElement();
        List<Node> list = document.selectNodes("//id");
        for (Node node : list) {
            if (node.getText().equals(id)) {
                Node parent = node.getParent();
                root.remove(parent);
            }
        }
        Dom4jUtils.write("student.xml", document);
    }

    // 根据ID查询学生信息。
    private static void selectStudentById(String ID) {
        Document document = Dom4jUtils.getDocument("student.xml");
        Element root = document.getRootElement();
        List<Node> list = document.selectNodes("//id");
        for (Node node : list) {
            if (node.getText().equals(ID)) {
                Element parent = node.getParent();
                Element id = parent.element("id");
                Element name = parent.element("name");
                Element age = parent.element("age");
                System.out.println(id.getText() + "--" + name.getText() + "--"
                        + age.getText());
            }
        }
    }

}
