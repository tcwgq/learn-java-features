package com.tcwgq.dom4j;

import com.tcwgq.utils.Dom4jUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Test;

import java.util.List;

/**
 * 封装dom4j的相关操作
 *
 * @author lenovo
 */
public class EditTest {
    /**
     * 封装dom4j的相关操作
     *
     * @throws Exception
     */
    @Test
    public void add() throws Exception {
        Document document = Dom4jUtils.getDocument("copy.xml");
        Element root = document.getRootElement();
        Element person = root.element("person");
        List<Element> list = person.elements();
        System.out.println(list);
        Element element = DocumentHelper.createElement("sex");
        element.addText("男");
        list.add(1, element);
        Dom4jUtils.write("copy.xml", document);
    }

    /**
     * 修改第一个person下面的age为30。
     */
    @Test
    public void editElement() {
        Document document = Dom4jUtils.getDocument("people.xml");
        Element root = document.getRootElement();
        Element person = root.element("person");
        Element name = person.element("name");
        name.setText("zhangSan");
        Element age = person.element("age");
        age.setText("30");
        Dom4jUtils.write("people.xml", document);
    }

    /**
     * 删除节点，例如把第一个person节点的sex节点删除。
     */
    @Test
    public void remove() {
        Document document = Dom4jUtils.getDocument("people.xml");
        Element root = document.getRootElement();
        Element person = root.element("person");
        Element sex = person.element("sex");
        person.remove(sex);
        Dom4jUtils.write("people.xml", document);
    }
}
