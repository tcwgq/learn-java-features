package com.tcwgq.dom4j;

import com.tcwgq.utils.Dom4jUtils;
import org.dom4j.Document;
import org.dom4j.Node;
import org.junit.Test;

import java.util.List;

/**
 * XPATH简介：
 * 实例一：基本的XPath语法类似于在一个文件系统中定位文件,如果路径以斜线 / 开始, 那么该路径就表示到一个元素的绝对路径。
 * 实例二：如果路径以双斜线 // 开头, 则表示选择文档中所有满足双斜线//之后规则的元素(无论层级关系)。
 * 实例三：星号 * 表示选择所有由星号之前的路径所定位的元素。
 * 实例四：方块号里的表达式可以进一步的指定元素, 其中数字表示元素在选择集里的位置, 而last()函数则表示选择集中的最后一个元素。
 * 实例五：//@id选择所有有ID属性的元素，//BBB[@id]选择所有有ID属性的BBB元素。
 * 实例六：属性的值可以被用来作为选择的准则, normalize-space函数删除了前部和尾部的空格, 并且把连续的空格串替换为一个单一的空格
 * <p>
 * 使用dom4j支持XPath操作 默认情况下，dom4j不支持xpath操作，使用之前需要导入相关的支持包
 * selectNodes("xpath表达式"):获取多大节点。 selectSingleNode("xpath表达式"):获取单个节点。
 * <p>
 * dom4j支持xpath的操作，获取第一个person下的name的值。
 * 观察可以发现，第一个person下面有id="20160330"属性，因此可以根据这个属性值获取这个person。
 * 使用xpath可以这样写：//person[@id="20160330"]/name
 *
 * @author lenovo
 */
public class XpathTest {
    @Test
    public void selectNodes() {
        Document document = Dom4jUtils.getDocument("people.xml");
        List<Node> list = document.selectNodes("//name");
        for (Node node : list) {
            System.out.println(node.getText());
        }
    }

    @Test
    public void selectSingleNode() {
        Document document = Dom4jUtils.getDocument("people.xml");
        Node name = document.selectSingleNode("//person[@id='20160330']/name");
        System.out.println(name.getText());
    }
}
