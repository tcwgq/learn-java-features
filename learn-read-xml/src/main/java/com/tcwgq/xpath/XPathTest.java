package com.tcwgq.xpath;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author tcwgq
 * @since 2020/1/15 14:35
 */
public class XPathTest {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //factory.setValidating(true);
        factory.setNamespaceAware(false);
        factory.setIgnoringComments(true);
        factory.setIgnoringElementContentWhitespace(true);
        factory.setCoalescing(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        builder.setEntityResolver(null);
        builder.setErrorHandler(new ErrorHandler() {
            @Override
            public void warning(SAXParseException exception) throws SAXException {
                System.out.println(exception.getMessage());
            }

            @Override
            public void error(SAXParseException exception) throws SAXException {
                System.out.println(exception.getMessage());
            }

            @Override
            public void fatalError(SAXParseException exception) throws SAXException {
                System.out.println(exception.getMessage());
            }
        });
        Document doc = builder.parse(new InputSource(new FileInputStream("src/main/resources/people.xml")));
        XPathFactory factory1 = XPathFactory.newInstance();
        XPath xPath = factory1.newXPath();
        // 获取根节点下的所有person节点
        XPathExpression compile = xPath.compile("//person");
        NodeList list = (NodeList) compile.evaluate(doc, XPathConstants.NODESET);
        int length = list.getLength();
        System.out.println("parent nodes length: " + length);
        for (int i = 0; i < length; i++) {
            Node parent = list.item(i);
            // 获取节点属性
            NamedNodeMap attributes = parent.getAttributes();
            Node idNode = attributes.getNamedItem("id");
            System.out.println(idNode.getTextContent());
            // 获取节点的子节点
            NodeList childNodes = parent.getChildNodes();
            int childNodesLength = childNodes.getLength();
            System.out.println("child nodes length: " + childNodesLength);
            for (int j = 0; j < childNodesLength; j++) {
                // 直接这样获取的话，会有特殊字符
                // String text = childNodes.item(j).getTextContent();
                Node item = childNodes.item(i);
                String nodeName = item.getNodeName();
                System.out.println(nodeName);
                //String textContent = item.getTextContent();
                //System.out.println(nodeName + "-" + textContent);
            }
        }
    }
}
