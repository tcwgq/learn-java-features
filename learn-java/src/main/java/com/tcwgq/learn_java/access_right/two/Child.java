package com.tcwgq.learn_java.access_right.two;

import com.tcwgq.learn_java.access_right.one.Parent;

class Child extends Parent {
    private Integer sex;

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public void a() {
        this.a = 1;
        this.b = 2;
        // this.c = 3; // 不可见
        // this.d = 4; // 不可见
        this.AA();
        this.BB();
        // this.CC();// 不可见
        // this.DD();// 不可见
    }

}
