package com.tcwgq.learn_java.access_right.two;

public class C {
    public void AA() {
        // A a = new A(); // 不可见
    }

    protected void BB() {

    }

    void CC() {

    }

    private void DD() {

    }
}
