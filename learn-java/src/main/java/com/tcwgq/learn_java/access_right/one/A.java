package com.tcwgq.learn_java.access_right.one;

class A {
    public int a;
    protected int b;
    int c;
    private int d;

    public void AA() {
        this.a = 1;
        this.b = 2;
        this.c = 3;
        this.d = 4;
        this.BB();
        this.CC();
        this.DD();
    }

    protected void BB() {

    }

    void CC() {

    }

    private void DD() {

    }
}
