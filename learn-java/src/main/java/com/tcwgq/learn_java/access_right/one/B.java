package com.tcwgq.learn_java.access_right.one;

public class B {
    public void AA() {
        A a = new A();
        a.a = 1;
        a.b = 2;
        a.c = 3;
        // a.d = 4; // 不可见
        a.AA();
        a.BB();
        a.CC();
        // a.DD(); // 不可见
    }

    protected void BB() {

    }

    void CC() {

    }

    private void DD() {

    }
}
