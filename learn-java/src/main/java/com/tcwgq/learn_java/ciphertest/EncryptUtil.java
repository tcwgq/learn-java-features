package com.tcwgq.learn_java.ciphertest;

import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class EncryptUtil {

    // 对称加密
    private static final String AES = "AES";
    private static final String DES = "DES";
    // 非对称加密算法
    private static final String RSA = "RSA";
    // 数字摘要算法
    private static final String SHA = "SHA";
    private static final String MD5 = "MD5";

    private static final String CHARSET = "UTF-8";

    @Test
    public void test1() throws Exception {
        String secret = "112113dasfasfafasfasfd";
        SecretKey key = new SecretKeySpec(secret.getBytes(), "AES");
        // 算法/模式/填充，不写的话，使用默认模式与填充
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        String a = "hello";
        byte[] reuslt = cipher.doFinal(a.getBytes());
        System.out.println(new String(reuslt));
    }

    @Test
    public void test2() throws NoSuchAlgorithmException, NoSuchPaddingException {
        KeyGenerator kg = KeyGenerator.getInstance("AES");
        SecretKey secretKey = kg.generateKey();
        Cipher c = Cipher.getInstance("AES");
    }

    @Test
    public void test3() {
        byte[] decode = Base64.getDecoder().decode("hello".getBytes());
        System.out.println(String.valueOf(decode));
    }
}
