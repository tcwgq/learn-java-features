package com.tcwgq.learn_java.ciphertest;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

/**
 * @Author wangguangqiang
 * @Date 2018年9月5日 下午4:00:39
 * @Email wangguangqiang@jd.com
 */
public class CipherTest {
	private static final String AES = "AES";
	private static final String DES = "DES";
	private static final String RSA = "RSA";

	/**
	 * 使用KeyGenerator
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAES() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 128; i++) {
			sb.append(new Random().nextInt(10));
		}
		String pwd = sb.toString();
		System.out.println(pwd);
		KeyGenerator keyGenerator = KeyGenerator.getInstance(AES);
		String name = keyGenerator.getProvider().getName();
		System.out.println(name);
		// size must be equal to 128, 192 or 256
		keyGenerator.init(128, new SecureRandom(pwd.getBytes()));
		SecretKey secretKey = keyGenerator.generateKey();
		// 返回基本编码格式的密钥，如果此密钥不支持编码，则返回 null。
		byte[] encoded = secretKey.getEncoded();
		System.out.println(Base64.encodeBase64String(encoded));
		// 返回此密钥的基本编码格式，如果此密钥不支持编码，则返回 null。
		String format = secretKey.getFormat();
		System.out.println(format);
		Cipher cipher = Cipher.getInstance(AES);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		String s = "hello";
		byte[] bytes = cipher.doFinal(s.getBytes());
		/**
		 * 1.会使用到JDK里sun.misc套件下的BASE64Encoder和BASE64Decoder 2.apache Commons
		 * Codec 3.Java 8的java.util套件中，新增了Base64类
		 */
		String base64String = Base64.encodeBase64String(bytes);
		System.out.println(base64String);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] doFinal = cipher.doFinal(bytes);
		System.out.println(new String(doFinal));
	}

	@Test
	public void testAES1() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 128; i++) {
			sb.append(new Random().nextInt(10));
		}
		String pwd = sb.toString();
		KeyGenerator keyGenerator = KeyGenerator.getInstance(AES);
		// size must be equal to 128, 192 or 256
		keyGenerator.init(128, new SecureRandom(pwd.getBytes()));
		SecretKey secretKey = keyGenerator.generateKey();
		// 返回基本编码格式的密钥，如果此密钥不支持编码，则返回 null。
		byte[] encoded = secretKey.getEncoded();
		// 此构造方法不检查给定的字节实际上是否指定了一个指定算法的密钥，不推荐使用这种形式，应该使用DESKeySpec
		SecretKeySpec keySpec = new SecretKeySpec(encoded, AES);
		Cipher cipher = Cipher.getInstance(AES);
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		String s = "hello";
		byte[] bytes = cipher.doFinal(s.getBytes());
		/**
		 * 1.会使用到JDK里sun.misc套件下的BASE64Encoder和BASE64Decoder 2.apache Commons
		 * Codec 3.Java 8的java.util套件中，新增了Base64类
		 */
		String base64String = Base64.encodeBase64String(bytes);
		System.out.println(base64String);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] doFinal = cipher.doFinal(bytes);
		System.out.println(new String(doFinal));
	}

	@Test
	public void testDES() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 56; i++) {
			sb.append(new Random().nextInt(10));
		}
		String pwd = sb.toString();
		System.out.println(pwd);
		KeyGenerator keyGenerator = KeyGenerator.getInstance(DES);
		keyGenerator.init(56, new SecureRandom(pwd.getBytes()));
		SecretKey secretKey = keyGenerator.generateKey();
		Cipher cipher = Cipher.getInstance(DES);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		String s = "hello";
		byte[] bytes = cipher.doFinal(s.getBytes());
		String base64String = Base64.encodeBase64String(bytes);
		System.out.println(base64String);
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] doFinal = cipher.doFinal(bytes);
		System.out.println(new String(doFinal));
	}

	/**
	 * 使用SecretKeyFactory，SecretKeyFactory是对称秘钥工厂
	 */
	@Test
	public void testDES1() throws Exception {
		SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(DES);
		String pwd = "11211388";
		// 创建一个 DESKeySpec 对象，使用 key 中的前 8 个字节作为 DES 密钥的密钥内容。
		// key长度不可小于8个字符
		DESKeySpec keySpec = new DESKeySpec(pwd.getBytes());
		SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
		Cipher cipher = Cipher.getInstance(DES);
		// 使用SecretKeySpec
		// SecretKeySpec secretKey = new SecretKeySpec(pwd.getBytes(), DES);
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] doFinal = cipher.doFinal("Hello".getBytes());
		System.out.println(Base64.encodeBase64String(doFinal));
	}

	@Test
	public void testDES2() throws Exception {
		String pwd = "11211388";
		SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DES");
		DESKeySpec keySpec = new DESKeySpec(pwd.getBytes());
		SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
		// 模式和填充需要调研下，PKCS5Padding，表示使用jdk自动填充
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		String s = "hahahaha";
		// Initialization Vector
		AlgorithmParameterSpec iv = new IvParameterSpec(s.getBytes());
		cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);
		byte[] doFinal = cipher.doFinal("Hello".getBytes());
		System.out.println(Base64.encodeBase64String(doFinal));
	}

	/**
	 * RSA加密算法对于加密数据的长度是有要求的。一般来说，明文长度小于等于密钥长度（Bytes）-11。
	 * 
	 * @throws Exception
	 */
	@Test
	public void testRSA() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 512; i++) {
			sb.append(new Random().nextInt(10));
		}
		String pwd = sb.toString();
		KeyPairGenerator generator = KeyPairGenerator.getInstance(RSA);
		generator.initialize(512, new SecureRandom(pwd.getBytes()));
		// 等同于genKeyPair
		KeyPair keyPair = generator.generateKeyPair();
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();
		Cipher cipher = Cipher.getInstance(RSA);
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);
		String msg = "hello world, java!";
		byte[] doFinal = cipher.doFinal(msg.getBytes());
		cipher.init(Cipher.DECRYPT_MODE, publicKey);
		byte[] doFinal2 = cipher.doFinal(doFinal);
		System.out.println(new String(doFinal2));
	}

	/**
	 * RSA公钥和私钥之间存在关系，人工构造麻烦，所以先使用KeyPairGenerator产生
	 * 
	 * @throws Exception
	 */
	@Test
	public void testRSA1() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 1024; i++) {
			sb.append(new Random().nextInt(10));
		}
		String pwd = sb.toString();
		// 设置keyPair
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(RSA);
		keyPairGenerator.initialize(1024, new SecureRandom(pwd.getBytes()));
		// 动态生成密钥对，这是当前最耗时的操作，一般要2s以上。
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();
		// 得到公私钥独立编码比特
		byte[] encodedPublic = publicKey.getEncoded();
		byte[] encodedPrivate = privateKey.getEncoded();
		// keyFactory
		KeyFactory keyFactory = KeyFactory.getInstance(RSA);
		// publicKey还原
		X509EncodedKeySpec x509 = new X509EncodedKeySpec(encodedPublic);
		PublicKey publicKey1 = keyFactory.generatePublic(x509);
		// privateKey还原
		PKCS8EncodedKeySpec pkcs8 = new PKCS8EncodedKeySpec(encodedPrivate);
		PrivateKey privateKey1 = keyFactory.generatePrivate(pkcs8);

	}

	@Test
	public void testRSA2() throws Exception {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 1024; i++) {
			sb.append(new Random().nextInt(10));
		}
		String pwd = sb.toString();
		// 设置keyPair
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(RSA);
		keyPairGenerator.initialize(1024, new SecureRandom(pwd.getBytes()));
		// 动态生成密钥对，这是当前最耗时的操作，一般要2s以上。
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		PublicKey publicKey = keyPair.getPublic();
		PrivateKey privateKey = keyPair.getPrivate();
		// 得到公私钥独立编码比特
		byte[] encodedPublic = publicKey.getEncoded();
		byte[] encodedPrivate = privateKey.getEncoded();
		// Base64编码
		System.out.println("publicKey: " + Base64.encodeBase64String(encodedPublic));
		System.out.println("publicKey length: " + encodedPublic.length);
		System.out.println("privateKey: " + Base64.encodeBase64String(encodedPrivate));
		System.out.println("privateKey length: " + encodedPrivate.length);
		RSAPublicKey rsaPublicKey = (RSAPublicKey) publicKey;
		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;
		System.out.println(rsaPublicKey.getModulus());// N
		System.out.println(rsaPrivateKey.getModulus()); // N
		// (N, e)是公钥
		System.out.println(rsaPublicKey.getPublicExponent());// e
		// (N, d)是私钥
		System.out.println(rsaPrivateKey.getPrivateExponent());// d

	}

	@Test
	public void testRSA3() throws Exception {
		String publicKeyString = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmpqGI5XKcjRXur/oAFt5fVxReND3hAw7M1LDs7Bl5DXyQv5gMvpNucsxUaO+NfuF5MCscnidF0Z2rYVyDx6+Zh+qElZ4gNRSDzbk12eF7UUpbPrmaBDIrbYEB9RGwLDvK0EohjB4ECZGHVaIFPKZRUz0lOiyrpDOXunyiIY7jQQIDAQAB";
		String privateKeyString = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKamoYjlcpyNFe6v+gAW3l9XFF40PeEDDszUsOzsGXkNfJC/mAy+k25yzFRo741+4XkwKxyeJ0XRnathXIPHr5mH6oSVniA1FIPNuTXZ4XtRSls+uZoEMittgQH1EbAsO8rQSiGMHgQJkYdVogU8plFTPSU6LKukM5e6fKIhjuNBAgMBAAECgYBy0zVSAeZqf7uw7oevMBKgVnuuPQi11UmJULNnS3Zxth0yDxFRFq6211p6fqMwN1BcFBhga4hbzwZ4Y2k/U4NlhRqQX3kxU8h8fpYF2t71dS9EwKJTRNblpT+IsnhkV9uNRkbpsxSJ0f+0a57gUqIGDpl/rF5m7JBdk79v3PPE0QJBANfnl/FHJ49zeqYUTeynUC8lyQBMxuCGVPEQhT57N1G7BQUjLbl3mMq9FbLNYlm4bQmER42JbsIEy9U7KrjyoBMCQQDFmXIPwGRPw4xqDPfLo1ef7YArvpkuizyq5dRNisE8pkBOhMRfE2BkNypjkOC9lveVIB+y8+daGj0wWyOR3KHbAkEAj86OggMm8oG0e9KiZLK80eqVC0IYZYyUH00lswxXOHUdTuKznglvF/sB7OqoS6Jv3hrhngCgaVLQgeK6/h19SQJAJPPtQMTZ/ZC0UaDbNln1qRLhRUz/mJSxY8RXT6kDOjRdElIB50XYYOVwJ+6QGOSCqIC5bN3wBgmiJ8jjfgOkwQJAAl6mX/WTEwERfYgqSgV8O6GiO37X+grP717YgSyn5O81GYiXwpU4UsCxxnDL/06FT3bxrFqL8gqn6FkV0desiw==";
		KeyFactory keyFactory = KeyFactory.getInstance(RSA);
		X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64.decodeBase64(publicKeyString));
		PublicKey publicKey = keyFactory.generatePublic(x509EncodedKeySpec);
		PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKeyString));
		PrivateKey privateKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);

	}

	@Test
	public void testRSA4() throws Exception {
		BigInteger N = new BigInteger(
				"94295037637443928317491486853019787106936025773287005448325811628674033900415605816163332796028016205105251466523697478910880834233468996066217809812735866830936605014763835282730252851261993904701755658704818991573393325952923719871052897389074446090165718898103948250026093143945907403191513949295051996761");
		BigInteger e = new BigInteger("65537");
		BigInteger d = new BigInteger(
				"13979440402908360277900228668751091010131535261199880143063209878148174517851565163340448013278120839354908269355390767125411877037587694978094393093070196863522720945954494019249393239645312240327608628477058316669772807168885418628429396718633489735778349657666598367278032478345816742359582118487296169873");
		KeyFactory keyFactory = KeyFactory.getInstance(RSA);
		RSAPublicKeySpec RSAPublicKeySpec = new RSAPublicKeySpec(N, e);
		RSAPrivateKeySpec RSAPrivateKeySpec = new RSAPrivateKeySpec(N, d);
		PublicKey publicKey = keyFactory.generatePublic(RSAPublicKeySpec);
		PrivateKey privateKey = keyFactory.generatePrivate(RSAPrivateKeySpec);

	}

	/**
	 * 各种产生key的方式到底有何差异？
	 */
	@Test
	public void testGenerateeKey() {
		try {
			byte[] keyBytes = "11211388".getBytes();
			// 第一种，Factory
			DESKeySpec keySpec = new DESKeySpec(keyBytes);
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key1 = keyFactory.generateSecret(keySpec);

			// 第二种, Generator
			KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
			keyGenerator.init(56, new SecureRandom(keyBytes));// key为8个字节，实际用了56位；
																// 后面随机数用key作为种子seed生成
			SecretKey key2 = keyGenerator.generateKey();

			// 第三种， SecretKeySpec
			SecretKey key3 = new SecretKeySpec(keyBytes, "DES");// SecretKeySpec类同时实现了Key和KeySpec接口

			// 打印
			System.out.println("key1：" + byteToHexString(key1.getEncoded()));// 3131323131323838
			System.out.println("key2：" + byteToHexString(key2.getEncoded()));// 73F146E5F1D508AE
			System.out.println("key3：" + byteToHexString(key3.getEncoded()));// 3131323131333838

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testProvider() {
		// 获取jvm支持的所有provider
		Provider[] providers = Security.getProviders();
		for (Provider provider : providers) {
			System.out.println(provider.getName());
		}
		// 获取指定名称的provider
		Provider provider = Security.getProvider("SUN");
		System.out.println(provider.getName());
	}

	@Test
	public void testSHA11() throws NoSuchAlgorithmException {
		/**
		 * MD2 MD5 SHA-1 SHA-256 SHA-384 SHA-512
		 */
		String s1 = "Hello world, java!";
		String s2 = "hello world, java!";
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		digest.update(s1.getBytes());
		// 完成最终的hash计算，digest被重置
		byte[] bytes = digest.digest();
		System.out.println(Base64.encodeBase64String(bytes));
		digest.update(s2.getBytes());
		byte[] bytes1 = digest.digest();
		System.out.println(Base64.encodeBase64String(bytes1));
		System.out.println(MessageDigest.isEqual(bytes, bytes1));
	}

	@Test
	public void testMD5() throws NoSuchAlgorithmException {
		/**
		 * MD2 MD5 SHA-1 SHA-256 SHA-384 SHA-512
		 */
		String s1 = "Hello world, java!";
		String s2 = "hello world, java!";
		MessageDigest digest = MessageDigest.getInstance("MD5");
		digest.update(s1.getBytes());
		// 完成最终的hash计算，digest被重置
		byte[] bytes = digest.digest();
		System.out.println(Base64.encodeBase64String(bytes));
		digest.update(s2.getBytes());
		byte[] bytes1 = digest.digest();
		System.out.println(Base64.encodeBase64String(bytes1));
		System.out.println(MessageDigest.isEqual(bytes, bytes1));
	}

	@Test
	public void testBase64() {
		String s = "Hello world, java!";
		String base64String = Base64.encodeBase64String(s.getBytes());
		System.out.println(base64String);// SGVsbG8gd29ybGQsIGphdmEh
		// 安全的url：转换+为-、/为_、将多余的=去掉
		String base64urlSafeString = Base64.encodeBase64URLSafeString(s.getBytes());
		System.out.println(base64urlSafeString);// SGVsbG8gd29ybGQsIGphdmEh
		byte[] bytes1 = Base64.decodeBase64(base64String);
		byte[] decodeBase64 = Base64.decodeBase64(base64urlSafeString);
		System.out.println(new String(bytes1));// Hello world, java!
		System.out.println(new String(decodeBase64));// Hello world, java!
	}

	@Test
	public void testBase641() {
		String s = "http://www.baidu.com/getUser?name=zhangSan&age=23";
		String base64String = Base64.encodeBase64String(s.getBytes());
		System.out.println(base64String);// aHR0cDovL3d3dy5iYWlkdS5jb20vZ2V0VXNlcj9uYW1lPXpoYW5nU2FuJmFnZT0yMw==
		String base64urlSafeString = Base64.encodeBase64URLSafeString(s.getBytes());
		System.out.println(base64urlSafeString);// aHR0cDovL3d3dy5iYWlkdS5jb20vZ2V0VXNlcj9uYW1lPXpoYW5nU2FuJmFnZT0yMw
		byte[] bytes1 = Base64.decodeBase64(base64String);
		byte[] decodeBase64 = Base64.decodeBase64(base64urlSafeString);
		System.out.println(new String(bytes1));// http://www.baidu.com/getUser?name=zhangSan&age=23
		System.out.println(new String(decodeBase64));// http://www.baidu.com/getUser?name=zhangSan&age=23
	}

	public static String byteToHexString(byte[] bytes) {
		StringBuffer sb = new StringBuffer(bytes.length);
		String sTemp;
		for (int i = 0; i < bytes.length; i++) {
			sTemp = Integer.toHexString(0xFF & bytes[i]);
			if (sTemp.length() < 2)
				sb.append(0);
			sb.append(sTemp.toUpperCase());
		}
		return sb.toString();
	}

	private static byte toByte(char c) {
		byte b = (byte) "0123456789ABCDEF".indexOf(c);
		return b;
	}
}
