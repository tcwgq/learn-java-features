package com.tcwgq.learn_java.niotest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;

public class WebClient {
    public static void main(String[] args) throws IOException {
        DatagramChannel channel = DatagramChannel.open();
        channel.configureBlocking(false);
        channel.bind(new InetSocketAddress(1234));
        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
        readBuffer.clear();
        channel.receive(readBuffer);
        readBuffer.flip();
        Selector selector = Selector.open();
        channel.register(selector, SelectionKey.OP_READ);
        while (true) {
            int select = selector.select();
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();
                if (selectionKey.isReadable()) {
                    channel.receive(readBuffer);
                    readBuffer.flip();
                    System.out.println(new String(readBuffer.array(), 0, readBuffer.limit()));
                    readBuffer.clear();
                }
            }
            iterator.remove();

        }
    }
}
