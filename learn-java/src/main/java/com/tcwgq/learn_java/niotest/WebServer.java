package com.tcwgq.learn_java.niotest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/**
 * Created by tcwgq on 2018/10/22 22:41.
 */
public class WebServer {
    public static void main(String[] args) throws IOException {
        DatagramChannel channel = DatagramChannel.open();
        channel.configureBlocking(false);
        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
        readBuffer.clear();
        readBuffer.put("This is from server!".getBytes());
        readBuffer.flip();
        while (true) {
            channel.send(readBuffer, new InetSocketAddress("localhost", 1234));
            readBuffer.rewind();
        }
    }
}
