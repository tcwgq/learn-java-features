package com.tcwgq.learn_java.niotest;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		String path = "D:/a.txt";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(path);
			FileChannel c = fis.getChannel();
			// 100bytef
			ByteBuffer bb = ByteBuffer.allocate(100);
			int length = -1;
			while ((length = c.read(bb)) != -1) {
				bb.clear();
				byte[] array = bb.array();
				System.out.write(array, 0, length);
				System.out.println();
				System.out.println(bb.limit() + "--" + bb.position() + "--" + bb.capacity());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Hello World!");
	}
}
