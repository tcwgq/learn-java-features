package com.tcwgq.learn_java.niotest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import org.junit.Test;

public class BufferTest {
    @Test
    public void testRead() throws IOException {
        FileInputStream fis = new FileInputStream("d:/a.txt");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 将数据读取到缓冲区
        int len = 0;
        while ((len = channel.read(buffer)) != -1) {
            // 清空缓冲区，让其从头开始存取数据
            System.out.println("capacity:" + buffer.capacity());
            System.out.println("offset:" + buffer.arrayOffset());
            System.out.println("hasRemaining:" + buffer.hasRemaining());
            System.out.println("isDirect:" + buffer.isDirect());
            System.out.println("isReadOnly:" + buffer.isReadOnly());
            System.out.println("limit:" + buffer.limit());
            System.out.println("position:" + buffer.position());
            System.out.println("remaining:" + buffer.remaining());
            System.out.println("===============================");
            buffer.clear();
            // 返回底层实现数组
            byte[] bytes = buffer.array();
            System.out.println(new String(bytes, 0, len));
        }
        fis.close();
    }

    /**
     * 相对获取方法
     *
     * @throws IOException
     */
    @Test
    public void testGet() throws IOException {
        FileInputStream fis = new FileInputStream("d:/a.txt");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 将数据读取到缓冲区
        int len = 0;
        while ((len = channel.read(buffer)) != -1) {
            System.out.println("len: " + len);
            buffer.flip();
            // 读取缓冲区的内容
            while (buffer.hasRemaining()) {
                // 相对get方式，position每次递增1
                System.out.print((char) buffer.get());
                // 注意以下方法每次读取2个字节
                //System.out.print(buffer.getChar());
            }
            buffer.compact();
        }
        fis.close();
    }

    @Test
    public void testWrap() throws IOException {
        String s = "Hello world, java!";
        ByteBuffer byteBuffer = ByteBuffer.wrap(s.getBytes());
        while (byteBuffer.hasRemaining()) {
            System.out.print((char) byteBuffer.get());
        }
    }

    @Test
    public void testWrite() throws IOException {
        FileOutputStream fos = new FileOutputStream("d:/b.txt", true);
        FileChannel writeChannel = fos.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        System.out.println("capacity: " + buffer.capacity() + " limit: " + buffer.limit() + " position: " + buffer.position());
        String msg = "Hello world, java!";
        //buffer = Charset.forName("UTF-8").encode(msg);
        int len = 0;
        buffer.put(msg.getBytes());
        // 可通过这种方式判断clear, fip等操作
        System.out.println("capacity: " + buffer.capacity() + " limit: " + buffer.limit() + " position: " + buffer.position());
        // 切换为写模式
        buffer.flip();
        while ((len = writeChannel.write(buffer)) != 0) {
            System.out.println(len);
        }
        System.out.println("capacity: " + buffer.capacity() + " limit: " + buffer.limit() + " position: " + buffer.position());
        // 写入换行
        buffer.clear();
        buffer.put(System.lineSeparator().getBytes());
        buffer.flip();
        writeChannel.write(buffer);
        writeChannel.close();
        fos.close();
    }

    @Test
    public void testAppend() throws IOException {
        FileInputStream fis = new FileInputStream("d:/a.txt");
        // true为追加写入
        FileOutputStream fos = new FileOutputStream("d:/c.txt", true);
        FileChannel readChannel = fis.getChannel();
        FileChannel writeChannel = fos.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int len = 0;
        while ((len = readChannel.read(buffer)) != -1) {
            // 切换为写模式
            buffer.flip();
            writeChannel.write(buffer);
            buffer.clear();
            System.out.println(len);
        }
        buffer.clear();
        buffer.put(System.lineSeparator().getBytes());
        buffer.flip();
        writeChannel.write(buffer);
        writeChannel.close();
        readChannel.close();
        fos.close();
        fis.close();
    }

    @Test
    public void clear() throws IOException {
        FileInputStream fis = new FileInputStream("d:/gc.log");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 将数据读取到缓冲区
        channel.read(buffer);
        // 清空缓冲区，让其从头开始存取数据
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        System.out.println("===============================");
        buffer.clear();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        fis.close();
    }

    @Test
    public void flip() throws IOException {
        FileInputStream fis = new FileInputStream("d:/gc.log");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 将数据读取到缓冲区
        channel.read(buffer);
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        System.out.println("===============================");
        // 写模式切换为读模式时使用
        buffer.flip();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        fis.close();
    }

    @Test
    public void rewind() throws IOException {
        FileInputStream fis = new FileInputStream("d:/gc.log");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 将数据读取到缓冲区
        channel.read(buffer);
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        System.out.println("===============================");
        // position置为0，让其从头开始读取数据
        buffer.rewind();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        fis.close();
    }

    @Test
    public void mark() throws IOException {
        FileInputStream fis = new FileInputStream("d:/gc.log");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 将数据读取到缓冲区
        channel.read(buffer);
        buffer.position(10);
        // mark = position = 10;
        buffer.mark();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        System.out.println("===============================");
        // position = 100;
        buffer.position(100);
        // mark = position = 100;
        buffer.mark();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        fis.close();
    }

    @Test
    public void reset() throws IOException {
        FileInputStream fis = new FileInputStream("d:/gc.log");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 将数据读取到缓冲区
        channel.read(buffer);
        buffer.position(10);
        // mark = position = 10;
        buffer.mark();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        System.out.println("===============================");
        // position = 100;
        buffer.position(100);
        // position = 10;
        buffer.reset();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        fis.close();
    }

    @Test
    public void compact() throws IOException {
        FileInputStream fis = new FileInputStream("d:/gc.log");
        FileChannel channel = fis.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1000);
        // 将数据读取到缓冲区
        channel.read(buffer);
        buffer.position(100);
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        System.out.println("===============================");
        // position = limit - position;
        buffer.compact();
        System.out.println("capacity:" + buffer.capacity());
        System.out.println("position:" + buffer.position());
        System.out.println("limit:" + buffer.limit());
        System.out.println("remaining:" + buffer.remaining());
        fis.close();
    }

}
