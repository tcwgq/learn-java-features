package com.tcwgq.learn_java.niotest;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by tcwgq on 2018/10/15 22:24.
 */
public class ClientTest {
    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();
        // sc.configureBlocking(false);
        sc.connect(new InetSocketAddress("127.0.0.1", 8080));

        ByteBuffer readBuffer = ByteBuffer.allocate(1024);
        ByteBuffer writeBuffer = ByteBuffer.allocate(1024);
        writeBuffer.put("hello".getBytes());
        writeBuffer.flip();

        while (true) {
            writeBuffer.rewind();
            sc.write(writeBuffer);
            readBuffer.clear();
            sc.read(readBuffer);
            readBuffer.flip();
            System.out.println(new String(readBuffer.array()));
        }
    }
}
