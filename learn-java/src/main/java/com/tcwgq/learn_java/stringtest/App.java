package com.tcwgq.learn_java.stringtest;

import java.util.*;
import java.util.concurrent.*;

/**
 * Hello world!
 */
public class App {
    private static final Object[] obj = {1, 2, 3};

    public static void main(String[] args) {
        String a = "abc";
        String b = "abc";
        String c = new String("abc");
        String d = new String("abc");
        System.out.println(a == b);// true
        System.out.println(a.equals(b));// true
        System.out.println(c == d);// false
        System.out.println(c.equals(d));// true
        System.out.println(a == c);// false
        System.out.println(b == c);// false
        System.out.println(a.equals(c));// true
        System.out.println(b.equals(c));// true
        List<String> list = new ArrayList<>();
        List<String> list1 = new LinkedList<>();
        Vector<String> list2 = new Vector<>();
        List<String> list3 = new Stack<>();
        obj[0] = "hello";
        String[] strs = {"hello", "world"};
        Class<?> componentType = strs.getClass().getComponentType();
        System.out.println(componentType.getName());
        Map<String, String> map = new HashMap<String, String>();
        Hashtable<Integer, String> table = new Hashtable<>();
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(100);
        ExecutorService service = new ThreadPoolExecutor(10, 20, 100l, TimeUnit.MILLISECONDS, queue,
                Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
        ThreadLocal<String> local = new ThreadLocal<>();

    }
}
