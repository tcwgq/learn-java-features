package com.tcwgq.learn_java.stringtest;

public class Test {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        StringBuffer buffer = new StringBuffer("hello");
        /**
         * 因为java参数传递采用的是值传递，对于基本类型的变量，相当于直接将变量进行了拷贝。
         * 原因在于java采用的是值传递，对于引用变量，传递的是引用的值，
         * 也就是说让实参和形参同时指向了同一个对象，因此让形参重新指向另一个对象对实参并没有任何影响
         */
        myClass.changeValue(buffer);
        System.out.println(buffer.toString());
    }
}

class MyClass {
    void changeValue(StringBuffer buffer) {
        buffer = new StringBuffer();
        buffer.append("world");
    }
}
