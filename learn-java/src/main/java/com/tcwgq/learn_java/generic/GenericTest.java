package com.tcwgq.learn_java.generic;

/**
 * 泛型类
 * 
 * @author admin
 *
 */
public class GenericTest {
	public static void main(String[] args) {
		Generic<String> g1 = new Generic<String>();
		Generic<Integer> g2 = new Generic<Integer>();
		g1.setParam("helloworld");
		System.out.println(g1.getParam());
		System.out.println(g1.getClass() == g2.getClass());
		System.out.println(g1.getClass().getName());// com.tcwgq.learn_java.generic.Generic
		System.out.println(g2.getClass().getName());// com.tcwgq.learn_java.generic.Generic
	}

}

class Generic<T> {
	private T param;

	public T getParam() {
		return param;
	}

	public void setParam(T param) {
		this.param = param;
	}

}