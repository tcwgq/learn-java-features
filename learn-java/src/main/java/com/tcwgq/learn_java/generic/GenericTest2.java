package com.tcwgq.learn_java.generic;

/**
 * 泛型接口的两种实现方式
 * 
 * @author admin
 *
 */
public class GenericTest2 {
	public static void main(String[] args) {
		A<Integer> a = new A<Integer>();
		a.setParam(100);
		System.out.println(a.getInfo());
		System.out.println("-------------------");
		B b = new B();
		System.out.println(b.getInfo());
	}
}

interface Generic2<T> {
	// 接口中的成员变量都是public static final的
	// public static T param;// Cannot make a static reference to the non-static
	// type T

	public T getInfo();
}

class A<T> implements Generic2<T> {
	private T param;

	public T getParam() {
		return param;
	}

	public void setParam(T param) {
		this.param = param;
	}

	@Override
	public T getInfo() {
		return param;
	}

}

class B implements Generic2<String> {

	@Override
	public String getInfo() {
		return "Helloworld";
	}

}