package com.tcwgq.learn_java.generic;

import java.util.ArrayList;
import java.util.List;

public class GenericTest4 {
	public static void main(String[] args) {
		List<Animal> list1 = new ArrayList<>();
		list1.add(new Animal());
		list1.add(new Cat());
		list1.add(new Dog());
		System.out.println("--------------------");
		// 添加的类型必须是和?类型一致，但不知道?是什么类型，所以编译会报错。
		/**
		 * 注意： 在Java集合框架中，对于参数值是未知类型的容器类，只能读取其中元素，不能像其中添加元素，
		 * 因为，其类型是未知，所以编译器无法识别添加元素的类型和容器的类型是否兼容，唯一的例外是NULL
		 */
		// 规定上界
		List<? extends Animal> list2 = new ArrayList<Cat>();
		// list2.add(new Animal());
		// list2.add(new Cat());
		// list2.add(new Dog());
		List<? super Cat> list3 = new ArrayList<Animal>();
		// list3.add(new Animal());
		// list3.add(new Cat());
		// list3.add(new Dog());
	}
}

class Animal {
	private String name;
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void act() {
		System.out.println("运动");
	}

	@Override
	public String toString() {
		return "Animal [name=" + name + ", age=" + age + "]";
	}

}

class Cat extends Animal {
	@Override
	public void act() {
		System.out.println("猫爱吃鱼");
	}
}

class Dog extends Animal {
	@Override
	public void act() {
		System.out.println("狗爱啃骨头");
	}
}

class Bird {
	public void act() {
		System.out.println("fly fly fly...");
	}
}
