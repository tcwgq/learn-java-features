package com.tcwgq.learn_java.generic;

/**
 * 泛型方法
 * 
 * @author admin
 *
 */
public class GenericTest3 {
	public static void main(String[] args) {
		Generic3 g = new Generic3();
		System.out.println(g.setParam("helloworld"));
		g.show(new Integer(100));
	}
}

class Generic3 {
	public <T> T setParam(T t) {
		return t;
	}

	public <T> void show(T t) {
		System.out.println(t);
	}
}