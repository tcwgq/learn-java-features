package com.tcwgq.learn_java.generic;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 泛型操作的put，get原则
 *
 * @author tcwgq
 * @time 2020/8/17 21:22
 */
public class PutGetTest {
    @Test
    public void test1() {
        List<? extends Number> list = new ArrayList<>();
        //list.add(1);// 报错
        Number number = list.get(0);// 正常，所以叫get原则
    }

    @Test
    public void test2() {
        List<? super Number> list = new ArrayList<>();
        list.add(1);// put原则
        list.add(2L);
        list.add(0.11);
        //Number object = list.get(0);// 会报错
        Object object = list.get(1);
        System.out.println(object);
    }
}
