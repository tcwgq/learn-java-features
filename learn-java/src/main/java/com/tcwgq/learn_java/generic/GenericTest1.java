package com.tcwgq.learn_java.generic;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 泛型只在编译阶段有效，运行阶段会泛型擦除。
 * 
 * @author admin
 *
 */
public class GenericTest1 {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("helloworld");
		Class<?> c = list.getClass();
		try {
			// 利用反射绕过了编译阶段，也就绕过了泛型检查
			Method method = c.getMethod("add", Object.class);
			method.invoke(list, 100);
			System.out.println(list);// [helloworld, 100]
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
