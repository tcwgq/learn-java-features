package com.tcwgq.learn_java.thread_exception_handle;

import java.util.concurrent.*;

public class ThreadTest {
    public static void main(String[] args) {
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(100);
        ExecutorService service = new ThreadPoolExecutor(
                10,
                20,
                100L,
                TimeUnit.MILLISECONDS,
                queue,
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());

        try {
            new Thread(new Runnable() {
                public void run() {
                    throw new RuntimeException();
                }
            }).start();
        } catch (RuntimeException e) {
            System.out.println("catch thread exception!");
        }

        Thread t = new Thread(new Runnable() {
            public void run() {
                throw new RuntimeException("fuck!");
            }
        });
        t.setUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
        t.start();
        System.out.println("thread end...");
    }
}
