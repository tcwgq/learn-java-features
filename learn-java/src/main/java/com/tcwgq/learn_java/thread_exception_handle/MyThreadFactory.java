package com.tcwgq.learn_java.thread_exception_handle;

import java.util.concurrent.ThreadFactory;

public class MyThreadFactory implements ThreadFactory {
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        t.setUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
        return t;
    }
}
