package com.tcwgq.learn_java.thread_exception_handle;

public class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    public void uncaughtException(Thread t, Throwable e) {
        System.out.println("uncaught: " + e);
    }
}
