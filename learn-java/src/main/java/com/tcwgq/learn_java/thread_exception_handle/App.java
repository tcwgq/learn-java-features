package com.tcwgq.learn_java.thread_exception_handle;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println(Integer.highestOneBit((16 - 1) << 1));
        System.out.println(15 << 1);
        System.out.println(Integer.bitCount(15));// 4
        // 1111 右移1位 11110 highestOneBit(11110)
        // int rounded = number >= MAXIMUM_CAPACITY ? MAXIMUM_CAPACITY :
        // (rounded = Integer.highestOneBit(number)) != 0 ? ((Integer.bitCount(number) > 1) ? rounded << 1 : rounded) : 1;
    }
}
