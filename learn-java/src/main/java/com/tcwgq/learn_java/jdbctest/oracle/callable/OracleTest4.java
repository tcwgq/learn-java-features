package com.tcwgq.learn_java.jdbctest.oracle.callable;

import java.sql.*;

/**
 * 调用有返回值的存储过程
 *
 * @author lenovo
 */
public class OracleTest4 {
    public static void main(String[] args) throws ClassNotFoundException,
            SQLException {
        // 主要记住驱动类名和url路径
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:orcl", "scott", "112113");
        CallableStatement cst = conn
                .prepareCall("{call get_page(?, ?, ?, ?, ?, ?)}");
        cst.setString(1, "emp");
        cst.setInt(2, 3);
        cst.setInt(3, 2);
        cst.registerOutParameter(4, oracle.jdbc.OracleTypes.NUMBER);
        cst.registerOutParameter(5, oracle.jdbc.OracleTypes.NUMBER);
        cst.registerOutParameter(6, oracle.jdbc.OracleTypes.CURSOR);
        cst.execute();
        int rows = cst.getInt(4);
        int pages = cst.getInt(5);
        System.out.println(rows);
        System.out.println(pages);
        ResultSet rs = (ResultSet) cst.getObject(6);
        while (rs.next()) {
            System.out.println(rs.getString(1) + "\t" + rs.getString(2) + "\t"
                    + rs.getString("sal"));
        }
        cst.close();
        conn.close();
    }
}
