package com.tcwgq.learn_java.jdbctest.oracle.callable;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 调用无返回值的存储过程
 *
 * @author lenovo
 */
public class OracleTest1 {
    public static void main(String[] args) throws ClassNotFoundException,
            SQLException {
        // 主要记住驱动类名和url路径
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:orcl", "scott", "112113");
        CallableStatement cst = conn.prepareCall("{call insert_book(?, ?, ?)}");
        cst.setInt(1, 1);
        cst.setString(2, "天龙八部");
        cst.setString(3, "上海文艺出版社");
        cst.execute();
        cst.close();
        conn.close();
    }
}
