package com.tcwgq.learn_java.jdbctest.oracle;

import java.sql.*;

/**
 * 使用JDBC连接oracle数据库(掌握)
 *
 * @author lenovo
 */
public class JdbcTest {
    public static void main(String[] args) throws SQLException,
            ClassNotFoundException {
        // 主要记住驱动类名和url路径
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:orcl", "scott", "112113");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select * from emp");
        while (rs.next()) {
            System.out.println(rs.getString("ename"));
        }
        rs.close();
        st.close();
        conn.close();
    }
}
