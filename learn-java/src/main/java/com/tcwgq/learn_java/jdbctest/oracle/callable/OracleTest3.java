package com.tcwgq.learn_java.jdbctest.oracle.callable;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 调用有返回值的存储过程
 *
 * @author lenovo
 */
public class OracleTest3 {
    public static void main(String[] args) throws ClassNotFoundException,
            SQLException {
        // 主要记住驱动类名和url路径
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:orcl", "scott", "112113");
        CallableStatement cst = conn
                .prepareCall("{call get_bookNameAndPublish(?, ?, ?)}");
        cst.setInt(1, 1);
        cst.registerOutParameter(2, oracle.jdbc.OracleTypes.VARCHAR);
        cst.registerOutParameter(3, oracle.jdbc.OracleTypes.VARCHAR);
        cst.execute();
        String bname = cst.getString(2);
        String bpublish = cst.getString(3);
        System.out.println(bname);
        System.out.println(bpublish);
        cst.close();
        conn.close();
    }
}
