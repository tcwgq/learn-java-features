package com.tcwgq.learn_java.jdbctest.oracle.callable;

import java.sql.*;

/**
 * 调用有返回值的存储过程
 *
 * @author lenovo
 */
public class OracleTest5 {
    public static void main(String[] args) throws ClassNotFoundException,
            SQLException {
        // 主要记住驱动类名和url路径
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:orcl", "scott", "112113");
        CallableStatement cst = conn.prepareCall("{call get_emp(?, ?)}");
        cst.setInt(1, 10);
        cst.registerOutParameter(2, oracle.jdbc.OracleTypes.CURSOR);
        cst.execute();
        ResultSet rs = (ResultSet) cst.getObject(2);
        while (rs.next()) {
            System.out.println(rs.getString(1) + "\t" + rs.getString(2) + "\t"
                    + rs.getString("sal"));
        }
        cst.close();
        conn.close();
    }
}
