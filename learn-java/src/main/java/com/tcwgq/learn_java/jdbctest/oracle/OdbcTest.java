package com.tcwgq.learn_java.jdbctest.oracle;

import java.sql.*;

/**
 * JDBC-ODBC桥连接oracle(了解即可)
 *
 * @author lenovo
 */
public class OdbcTest {
    public static void main(String[] args) throws ClassNotFoundException,
            SQLException {
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        Connection conn = DriverManager.getConnection("jdbc:odbc:oracle",
                "scott", "112113");
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("select * from emp");
        while (rs.next()) {
            System.out.println(rs.getBoolean("ename"));
        }
        rs.close();
        st.close();
        conn.close();
    }
}
