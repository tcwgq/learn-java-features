package com.tcwgq.learn_java.jdbctest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}

	public static void testJdbc() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jifenpay", "tcwgq", "112113");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select * from account limit 10");
		while (rs.next()) {
			System.out.println(rs.getInt(1) + "--" + rs.getString(2));
		}

	}

	public static void testJdbc1() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jifenpay", "tcwgq", "112113");
		String sql = "select * from account where id = ?";
		PreparedStatement st = conn.prepareStatement(sql);
		st.setInt(1, 1);
		ResultSet rs = st.executeQuery();
		while (rs.next()) {
			System.out.println(rs.getInt(1) + "--" + rs.getString(2));
		}

	}
}
