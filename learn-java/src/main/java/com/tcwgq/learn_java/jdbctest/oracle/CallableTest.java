package com.tcwgq.learn_java.jdbctest.oracle;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 调用存储过程
 *
 * @author lenovo
 */
public class CallableTest {
    public static void main(String[] args) throws SQLException,
            ClassNotFoundException {
        // 主要记住驱动类名和url路径
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:1521:orcl", "scott", "112113");
        CallableStatement cst = conn.prepareCall("{call update_stu(?, ?)}");
        cst.setInt(1, 1);
        cst.setString(2, "zhangSan");
        cst.execute();
        cst.close();
        conn.close();
    }
}
