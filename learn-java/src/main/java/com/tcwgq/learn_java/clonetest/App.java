package com.tcwgq.learn_java.clonetest;

import java.util.*;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws CloneNotSupportedException {
        User u1 = new User("zhangSan", 23);
        Person p1 = new Person(21);
        u1.setP(p1);
        System.out.println(u1);
        User u2 = (User) u1.clone();
        u2.getP().setAge(24);
        System.out.println("**************");
        System.out.println(u1);
        System.out.println(u2);
        List<String> list = new ArrayList<>();
        Map<String, String> map = new HashMap<>();
        Set<String> set = new HashSet<String>();
    }
}
