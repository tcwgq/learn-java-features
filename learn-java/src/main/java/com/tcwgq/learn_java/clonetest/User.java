package com.tcwgq.learn_java.clonetest;

public class User implements Cloneable {
	private String name;
	private int age;
	private Person p;

	public User() {
		super();
	}

	public User(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Person getP() {
		return p;
	}

	public void setP(Person p) {
		this.p = p;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		User clone = (User) super.clone();
		Person clone2 = (Person) p.clone();// 深度克隆
		clone.setP(clone2);
		return clone;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", age=" + age + ", p=" + p + "]";
	}

}
