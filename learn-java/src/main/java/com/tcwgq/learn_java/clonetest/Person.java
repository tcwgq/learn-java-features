package com.tcwgq.learn_java.clonetest;

public class Person implements Cloneable {
	private int age;

	public Person() {
		super();
	}

	public Person(int age) {
		super();
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public String toString() {
		return "Person [age=" + age + "]";
	}

}
