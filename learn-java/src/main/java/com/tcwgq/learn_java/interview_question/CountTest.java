package com.tcwgq.learn_java.interview_question;


/**
 * count的值为什么一直为0？
 *
 * @author lenovo
 */
public class CountTest {
    public static void main(String[] args) {
        int count = 0;
        for (int i = 0; i < 10; i++) {
            count = count++;
            System.out.println(count);
        }
        System.out.println(count);
    }
}
