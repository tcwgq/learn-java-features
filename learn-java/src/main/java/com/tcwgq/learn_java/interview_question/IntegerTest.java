package com.tcwgq.learn_java.interview_question;

/**
 * 以下程序的输出结果是什么？
 *
 * @author lenovo
 */
public class IntegerTest {
    public static void main(String[] args) {
        Integer i = new Integer(100);
        Integer j = new Integer(100);
        System.out.println(i == j);
        System.out.println(i > j);
        System.out.println(i < j);
    }
}
