package com.tcwgq.learn_java.interview_question;

import org.junit.Test;

import java.util.ArrayList;

/**
 * 数组有没有length()方法呢？字符串有没有length()方法呢？集合有没有length()方法呢？
 */
public class LengthDemo {
    @Test
    public void fun() {
        int[] i = new int[5];
        String s = "hello";
        ArrayList<String> al = new ArrayList<>();
        al.add("hello");
        al.add("world");
        al.add("java");
        System.out.println(i.length);
        System.out.println(s.length());
        System.out.println(al.size());
    }
}
