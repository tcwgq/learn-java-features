package com.tcwgq.learn_java.interview_question;

import org.junit.Test;

/**
 * 以下程序的输出结果是什么？
 *
 * @author lenovo
 */
public class GenericTest {
    class Foo<T> {
        public void show1(T t) {
            System.out.println(t.getClass());
        }

        public <T> void show2(T t) {
            System.out.println(t.getClass());
        }
    }

    @Test
    public void test1() {
        Foo<String> foo = new Foo<>();
        foo.show1("1");
        //foo.show1(1);
        //foo.show1('1');
        foo.show2("1");
        foo.show2(1);
        foo.show2('1');
    }
}
