package com.tcwgq.learn_java.interview_question;

import java.util.ArrayList;

/**
 * 去除数组中的重复元素，数组元素无序
 *
 * @author lenovo
 */
public class GetRid {
    public static void main(String[] args) {
        int[] a = {1, 3, 10, 8, 45, 20, 3, 10, 8, 69, 10, 8, 0};
        int[] b = Rid(a);
        for (int c : b) {
            System.out.println(c);
        }
    }

    public static int[] Rid(int[] a) {
        ArrayList<Integer> al = new ArrayList<>();
        for (int i = 0; i < a.length; i++) {
            if (!al.contains(a[i])) {
                al.add(a[i]);
            }
        }
        int length = al.size();
        int[] b = new int[length];
        for (int i = 0; i < length; i++) {
            b[i] = al.get(i);
        }
        return b;
    }

}
