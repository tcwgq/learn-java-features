package com.tcwgq.learn_java.interview_question;

/**
 * 以下程序的输出结果是什么？
 *
 * @author lenovo
 */
public class FloatTest {
    public static void main(String[] args) {
        float f = 1.234f;
        float r = f / 0;
        System.out.println(r);
    }
}
