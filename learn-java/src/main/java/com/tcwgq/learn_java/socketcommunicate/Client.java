package com.tcwgq.learn_java.socketcommunicate;

import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @author tcwgq
 * @time 2020/9/14 10:10
 */
public class Client {
    @Test
    public void test1() throws IOException {
        Socket socket = new Socket("localhost", 8081);
        OutputStream os = socket.getOutputStream();
        os.write("Hello world".getBytes(StandardCharsets.UTF_8));
        os.close();
        socket.close();
    }
}