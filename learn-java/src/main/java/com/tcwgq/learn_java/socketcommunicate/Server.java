package com.tcwgq.learn_java.socketcommunicate;


import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @author tcwgq
 * @time 2020/9/14 10:16
 */
public class Server {
    @Test
    public void test1() throws IOException {
        ServerSocket ss = new ServerSocket(8081);
        while (true) {
            Socket socket = ss.accept();
            InputStream is = socket.getInputStream();
            String content = read(is);
            System.out.println(content);
            socket.shutdownInput();
            is.close();
            // socket.close();
        }
    }

    private String read(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        int len;
        StringBuilder sb = new StringBuilder();
        while ((len = is.read(buffer)) != -1) {
            sb.append(new String(buffer, 0, len, StandardCharsets.UTF_8));
        }
        return sb.toString();
    }
}
