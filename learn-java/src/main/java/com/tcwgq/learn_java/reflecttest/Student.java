package com.tcwgq.learn_java.reflecttest;

public class Student {
	@AnnoParam(name = "zhangSan", value = "23")
	private String name;
	@AnnoParam(name = "lisi", value = "liSi")
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void show(String part) {
		System.out.println("漏" + "--" + part);
	}

	@AnnoMethod(name = "fuck", value = "fuck")
	public void fuck(@AnnoParam(name = "ximen", value = "daguanren") String male, @AnnoParam(name = "panjin", value = "xiaonvzi") String female) {
		System.out.println(male + " fuck " + female);
	}

	@Override
	public String toString() {
		return "Student{" +
				"name='" + name + '\'' +
				", age=" + age +
				'}';
	}
}
