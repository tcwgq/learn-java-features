package com.tcwgq.learn_java.reflecttest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.junit.Test;

public class ReflectTest {
	@Test
	public void testField() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> clazz = Class.forName("com.tcwgq.learn_java.reflecttest.Student");
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			System.out.println(field.getName());
			AnnoParam annoParam = field.getAnnotation(AnnoParam.class);
			if (annoParam == null) {
				continue;
			}
			System.out.println(annoParam.name() + "--" + annoParam.value());
		}
	}

	@Test
	public void testMethod() throws ClassNotFoundException {
		Class<?> clazz = Class.forName("com.tcwgq.learn_java.reflecttest.Student");
		Method[] methods = clazz.getDeclaredMethods();
		for (Method method : methods) {
			AnnoMethod annoMethod = method.getAnnotation(AnnoMethod.class);
			if (annoMethod == null) {
				continue;
			}
			System.out.println(annoMethod.name() + "--" + annoMethod.value());
			// 获取每个参数，及其上的注解，一个参数上可能有多个注解
			Annotation[][] annotations = method.getParameterAnnotations();
			// 获取参数类型
			Class<?>[] clazzes = method.getParameterTypes();
			System.out.println("参数的长度为" + clazzes.length);
			System.out.println(annotations.length);
			for (int i = 0; i < annotations.length; i++) {
				Annotation[] annos = annotations[i];
				if (annos.length == 0) {
					continue;
				}
				System.out.println(annos);
				// 获取参数类型
				System.out.println(clazzes[i].getName());
				for (int j = 0; j < annos.length; j++) {
					if (annos[j].annotationType().equals(AnnoParam.class)) {
						AnnoParam param = (AnnoParam) annos[j];
						System.out.println(param.name() + "--" + param.value());
					}
				}
			}
		}
	}

	@Test
	public void testSpring() {
		
	}
}
