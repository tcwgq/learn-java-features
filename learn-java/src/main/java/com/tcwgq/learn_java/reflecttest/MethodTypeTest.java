package com.tcwgq.learn_java.reflecttest;

import org.junit.Test;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;

/**
 * java7新特性
 * Created by wangguangqiang on 2019/
 * 10/17 17:40
 */
public class MethodTypeTest {
    @Test
    public void test1() throws Throwable {
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType methodType = MethodType.methodType(String.class, char.class, char.class);
        MethodHandle replace = lookup.findVirtual(String.class, "replace", methodType);
        Object o = replace.invokeWithArguments("hello", 'l', 'h');
        System.out.println(o);
    }

    @Test
    public void test2() throws Throwable {
        System.out.println(int.class);
        MethodHandles.Lookup lookup = MethodHandles.lookup();
        MethodType methodType = MethodType.methodType(void.class, int.class);
        MethodHandle replace = lookup.findVirtual(Student.class, "setAge", methodType);
        Student student = new Student();
        //replace.invokeWithArguments(student, 1);
        // 自动拆装箱，如果不自动拆装箱的话，会报错
        replace.invokeExact(student, Integer.parseInt("1"));
        System.out.println(student);
    }
}
