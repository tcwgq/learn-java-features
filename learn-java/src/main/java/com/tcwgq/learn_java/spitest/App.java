package com.tcwgq.learn_java.spitest;

import java.util.ServiceLoader;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ServiceLoader<IShout> s = ServiceLoader.load(IShout.class);
        for (IShout iShout : s) {
            iShout.shout();
        }
    }
}
