package com.tcwgq.learn_java.initialize;

public class Parent {
    static {
        System.out.println("Parent static...");
    }

    {
        System.out.println("Parent 构造代码块...");
    }

    public Parent() {
        System.out.println("Parent constructor...");
    }

    public void show() {
        System.out.println("Parent 一般方法...");
    }

    public static void method() {
        System.out.println("Parent 静态方法...");
    }
}
