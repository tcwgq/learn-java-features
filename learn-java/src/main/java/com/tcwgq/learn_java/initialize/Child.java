package com.tcwgq.learn_java.initialize;

public class Child extends Parent {
    static {
        System.out.println("Child static...");
    }

    {
        System.out.println("Child 构造代码块...");
    }

    public Child() {
        System.out.println("Child constructor...");
    }

    public void show() {
        System.out.println("Child 一般方法...");
    }

    public static void method() {
        System.out.println("Child 静态方法...");
    }
}
