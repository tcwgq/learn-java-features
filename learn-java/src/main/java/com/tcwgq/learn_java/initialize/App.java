package com.tcwgq.learn_java.initialize;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Parent p = new Child();// 先加载类，再初始化父类对象，最后初始化子类对象
        p.show();
        p.method();
        System.out.println("..............");
        p = new Child();
        p.show();
        p.method();// 静态方法不能重写
        Lock lock = new ReentrantLock();
        AtomicInteger ai = new AtomicInteger();
        System.out.println(getInt1());// 5
        System.out.println(getInt2());// 3
        System.out.println(getInt3());// hello
        System.out.println(getInt4(5));// 5
        System.out.println(getInt5(5));// 6
        System.out.println(getInt6(5));// 6
    }

    public static int getInt1() {
        try {
            return 4;
        } finally {
            return 5;
        }
    }

    public static int getInt2() {
        int a = 3;
        try {
            return a;
        } finally {
            a = 4;
        }
    }

    public static String getInt3() {
        String s = null;
        try {
            s = "hello";
            return s;
        } finally {
            s = "world";
        }
    }

    public static int getInt4(int a) {
        return a++;
    }

    public static int getInt5(int a) {
        try {
            ++a;
            return a++;// 先取值后增加
        } finally {
            a++;
        }
    }

    public static int getInt6(int a) {
        try {
            ++a;
        } finally {
            return a++;
        }
    }

}
