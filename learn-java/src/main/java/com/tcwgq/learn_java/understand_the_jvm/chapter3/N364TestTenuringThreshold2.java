package com.tcwgq.learn_java.understand_the_jvm.chapter3;

import org.junit.Test;

/**
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=15 -XX:+PrintTenuringDistribution
 *
 * @author tcwgq
 * @time 2020/9/12 18:24
 */
public class N364TestTenuringThreshold2 {
    private static final int _1M = 1024 * 1024;

    @Test
    public void testTenuringThreshold2() {
        byte[] al1, al2, al3, al4;
        al1 = new byte[_1M / 4];
        // al1+al2大于survivor空间一半
        al2 = new byte[_1M / 4];
        al3 = new byte[4 * _1M];
        al4 = new byte[4 * _1M];
        al4 = null;
        al4 = new byte[4 * _1M];
    }
}
