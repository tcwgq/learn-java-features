package com.tcwgq.learn_java.understand_the_jvm;

import org.junit.Test;
import org.openjdk.jol.info.ClassLayout;

/**
 * 对象头结构
 *
 * @author tcwgq
 * @time 2020/9/23 22:23
 */
public class ObjectHeader {
    @Test
    public void test1() {
        Object obj = new Object();
        Object[] objs = new Object[16];
        System.out.println(ClassLayout.parseInstance(obj).toPrintable());
        System.out.println(ClassLayout.parseInstance(objs).toPrintable());
    }
}
