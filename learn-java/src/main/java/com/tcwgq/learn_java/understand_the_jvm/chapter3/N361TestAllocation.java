package com.tcwgq.learn_java.understand_the_jvm.chapter3;

import org.junit.Test;

/**
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -XX:+PrintGCDetails -XX:SurvivorRatio=8
 *
 * @author tcwgq
 * @time 2020/9/12 17:55
 */
public class N361TestAllocation {
    private static final int _1M = 1024 * 1024;

    @Test
    public void testAllocation() {
        byte[] al1, al2, al3, al4;
        al1 = new byte[2 * _1M];
        al2 = new byte[2 * _1M];
        al3 = new byte[2 * _1M];
        al4 = new byte[4 * _1M]; // 出现1次MinorGC
    }
}
