package com.tcwgq.learn_java.understand_the_jvm.chapter3;

import org.junit.Test;

/**
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:MaxTenuringThreshold=1 -XX:+PrintTenuringDistribution
 *
 * @author tcwgq
 * @time 2020/9/12 18:24
 */
public class N363TestTenuringThreshold {
    private static final int _1M = 1024 * 1024;

    @Test
    public void testTenuringThreshold() {
        byte[] al1, al2, al3;
        al1 = new byte[_1M / 4];
        // 什么时候进入老年代取决于XX:MaxTenuringThreshold设置
        al2 = new byte[4 * _1M];
        al3 = new byte[4 * _1M];
        al3 = null;
        al3 = new byte[4 * _1M];
    }
}
