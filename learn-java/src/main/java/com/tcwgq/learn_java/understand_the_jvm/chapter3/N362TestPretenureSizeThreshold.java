package com.tcwgq.learn_java.understand_the_jvm.chapter3;

import org.junit.Test;

/**
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:PretenureSizeThreshold=3145728
 *
 * @author tcwgq
 * @time 2020/9/12 18:07
 */
public class N362TestPretenureSizeThreshold {
    private static final int _1M = 1024 * 1024;

    @Test
    public void testPretenureSizeThreshold() {
        byte[] al1;
        al1 = new byte[4 * _1M];
    }
}
