package com.tcwgq.learn_java.understand_the_jvm.chapter3;

import org.junit.Test;

/**
 * 请使用jdk1.6 update 24之前版本
 * <p>
 * -verbose:gc -Xms20M -Xmx20M -Xmn10M -XX:+UseSerialGC -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:-HandlePromotionFailure
 * <p>
 * 在jdk1.6 update 24之后-XX:-HandlePromotionFailure不起作用了，只要老年代的连续空间大于新生代对象的总大小或者历次晋升到老年代的对象的平均大小就进行MonitorGC，否则FullGC
 *
 * @author tcwgq
 * @time 2020/9/12 18:24
 */
public class N365TestHandlePromotion {
    private static final int _1M = 1024 * 1024;

    @Test
    public void testHandlePromotion() {
        byte[] al1, al2, al3, al4, al5, al6, al7;
        al1 = new byte[2 * _1M];
        al2 = new byte[2 * _1M];
        al3 = new byte[2 * _1M];
        al1 = null;
        al4 = new byte[2 * _1M];
        al5 = new byte[2 * _1M];
        al6 = new byte[2 * _1M];
        al4 = null;
        al5 = null;
        al6 = null;
        al7 = new byte[2 * _1M];
    }
}
