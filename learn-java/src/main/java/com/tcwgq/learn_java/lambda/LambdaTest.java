package com.tcwgq.learn_java.lambda;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.JButton;

import org.junit.Test;

/**
 * lambda表达式的结构 参数可以是零个或多个 参数类型可指定，可省略（根据表达式上下文推断） 参数包含在圆括号中，用逗号分隔
 * 只有一个参数时，圆括号可以省略 表达式主体可以是零条或多条语句,包含在花括号中 表达式主体只有一条语句时,花括号可省略
 * 表达式主体有一条以上语句时，表达式的返回类型与代码块的返回类型一致 表达式只有一条语句时，表达式的返回类型与该语句的返回类型一致
 * 
 * @author tcwgq
 * @time 2017年10月23日下午3:10:40
 * @email qiang.wang@guanaitong.com
 */
public class LambdaTest {
    // 例1、用lambda表达式实现Runnable
    @Test
    public void test() {
        /**
         * lambda表达式中，参数的类型可省略。Java编译器根据表达式的上下文推导出参数的类型。 //零个 ()->
         * System.out.println("no argument");
         * 
         * //一个 x->x+1
         * 
         * //两个 (x,y)->x+y
         * 
         * //省略参数类型 View.OnClickListener oneArgument =
         * view->Log.d(TAG,"one argument"); //指定参数类型 View.OnClickListener
         * oneArgument = (View view)->Log.d(TAG,"one argument");
         * 
         * //多行语句 //返回类型是代码块返回的void View.OnClickListener multiLine = (View
         * view)->{ Log.d(TAG,"multi statements"); Log.d(TAG,"second line"); }
         * 
         * //返回类型是表达式主体语句的返回类型int (int x)->x+1
         */

        // 原始的实现
        new Thread(new Runnable() {

            @Override
            public void run() {
                System.out.println("Before Java8, too much code for too little to do");
            }
        }).start();

        // lambda实现
        // (params) -> expression
        // (params) -> statement
        // (params) -> {statements}
        new Thread(() -> System.out.println("In Java8, Lambda expression rocks !!")).start();

    }

    // 例2、使用Java 8 lambda表达式进行事件处理
    @Test
    public void test1() {
        // before java8
        JButton show = new JButton("show");
        show.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Event handling without lambda expression is boring");
            }

        });

        // after java8
        show.addActionListener((ActionEvent e) -> {
            System.out.println("Light, Camera, Action !! Lambda expressions Rocks");
        });
    }

    // 例3、使用lambda表达式对列表进行迭代
    @Test
    public void test2() {
        // before java8
        List<String> features = Arrays.asList("Lambdas", "Default Method", "Stream API", "Date and Time API");
        for (String feature : features) {
            System.out.println(feature);
        }

        // after java8
        features.forEach(feature -> System.out.println(feature));

        /**
         * // 使用Java 8的方法引用更方便，方法引用由::双冒号操作符标示， // 看起来像C++的作用域解析运算符
         */
        features.forEach(System.out::println);
    }

    @Test
    public void test3() {
        List<String> languages = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");

        System.out.println("Languages which starts with J :");
        filter(languages, (String str) -> str.startsWith("J"));

        System.out.println("Languages which ends with a ");
        filter(languages, (String str) -> str.endsWith("a"));

        System.out.println("Print all languages :");
        filter(languages, (str) -> true);

        System.out.println("Print no language : ");
        filter(languages, (str) -> false);

        System.out.println("Print language whose length greater than 4:");
        filter(languages, (str) -> str.length() > 4);
    }

    private static void filter(List<String> names, Predicate<String> condition) {
        for (String name : names) {
            if (condition.test(name)) {
                System.out.println(name + " ");
            }
        }
    }

    // 更好的办法
    // Stream API的过滤方法也接受一个Predicate，这意味着可以将我们定制的 filter()
    // 方法替换成写在里面的内联代码，这就是lambda表达式的魔力。
    private static void filter1(List<String> names, Predicate<String> condition) {
        names.stream().filter(name -> condition.test(name)).forEach(name -> System.out.println(name + " "));
    }

    /**
     * java.util.function.Predicate 允许将两个或更多的 Predicate
     * 合成一个。它提供类似于逻辑操作符AND和OR的方法，名字叫做and()、or()和xor()，用于将传入 filter() 方法的条件合并起来
     * // 甚至可以用and()、or()和xor()逻辑函数来合并Predicate， //
     * 例如要找到所有以J开始，长度为四个字母的名字，你可以合并两个Predicate并传入
     * 
     * @param names
     * @param condition
     */
    private static void filter2(List<String> names, Predicate<String> condition) {
        Predicate<String> startsWithJ = s -> s.startsWith("J");
        Predicate<String> fourLetterLong = s -> s.length() == 4;
        names.stream().filter(startsWithJ.and(fourLetterLong)).forEach(name -> System.out.println(name + " "));
    }

    // 例5、如何在lambda表达式中加入Predicate
    @Test
    public void test4() {
        // filter2();
    }

    // 例6、Java 8中使用lambda表达式的Map和Reduce示例
    @Test
    public void test5() {
        // 不使用lambda表达式为每个订单加上12%的税
        List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
        for (Integer cost : costBeforeTax) {
            // 0.12竟然可以这么写
            double price = cost + .12 * cost;
            System.out.println(price);
        }

        /**
         * 本例介绍最广为人知的函数式编程概念map。它允许你将对象进行转换。例如在本例中，我们将 costBeforeTax
         * 列表的每个元素转换成为税后的值。我们将 x -> x*x lambda表达式传到 map() 方法，后者将其应用到流中的每一个元素。然后用
         * forEach() 将列表元素打印出来。使用流API的收集器类，可以得到所有含税的开销。有 toList() 这样的方法将 map
         * 或任何其他操作的结果合并起来。由于收集器在流上做终端操作，因此之后便不能重用流了。
         */
        // 使用lambda表达式
        costBeforeTax.stream().map(cost -> cost + .12 * cost).forEach(System.out::println);
    }

    // Java 8中使用lambda表达式的Map和Reduce示例
    @Test
    public void test6() {
        // 为每个订单加上12%的税
        // 老方法：
        List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
        double total = 0;
        for (Integer cost : costBeforeTax) {
            double price = cost + .12 * cost;
            total = total + price;
        }
        System.out.println("Total : " + total);

        Double bill = costBeforeTax.stream().map(cost -> cost + cost * .12).reduce((sum, cost) -> sum + cost).get();
        System.out.println("Total : " + bill);
    }

    // 例7、通过过滤创建一个String列表
    /**
     * 过滤是Java开发者在大规模集合上的一个常用操作，而现在使用lambda表达式和流API过滤大规模数据集合是惊人的简单。流提供了一个
     * filter() 方法，接受一个 Predicate 对象，即可以传入一个lambda表达式作为过滤逻辑。
     */
    @Test
    public void test7() {
        // 创建一个字符串列表，每个字符串长度大于2
        List<String> strList = Arrays.asList("abc", "bcd", "defg", "jk");

        List<String> filtered = strList.stream().filter(x -> x.length() > 2).collect(Collectors.toList());
        System.out.printf("Original List : %s, filtered list : %s %n", strList, filtered);
    }

    // 例8、对列表的每个元素应用函数
    /**
     * 我们通常需要对列表的每个元素使用某个函数，例如逐一乘以某个数、除以某个数或者做其它操作。这些操作都很适合用 map()
     * 方法，可以将转换逻辑以lambda表达式的形式放在 map() 方法里，就可以对集合的各个元素进行转换了
     */
    @Test
    public void test8() {
        // 将字符串换成大写并用逗号链接起来
        List<String> G7 = Arrays.asList("USA", "Japan", "France", "Germany", "Italy", "U.K.", "Canada");
        // 产生一个新的集合
        String G7Countries = G7.stream().map(x -> x.toUpperCase()).collect(Collectors.joining(", "));
        System.out.println(G7Countries);
    }

    // 例9、复制不同的值，创建一个子列表
    /**
     * 本例展示了如何利用流的 distinct() 方法来对集合进行去重。
     */
    @Test
    public void test9() {
        // 用所有不同的数字创建一个正方形列表
        List<Integer> numbers = Arrays.asList(9, 10, 3, 4, 7, 3, 4);
        List<Integer> distinct = numbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
        System.out.printf("Original List : %s,  Square Without duplicates : %s %n", numbers, distinct);
    }

    // 例10、计算集合元素的最大值、最小值、总和以及平均值
    /**
     * IntStream、LongStream 和 DoubleStream 等流的类中，有个非常有用的方法叫做 summaryStatistics()
     * 。可以返回 IntSummaryStatistics、LongSummaryStatistics 或者
     * DoubleSummaryStatistic s，描述流中元素的各种摘要数据。在本例中，我们用这个方法来计算列表的最大值和最小值。它也有
     * getSum() 和 getAverage() 方法来获得列表的所有元素的总和及平均值。
     */
    @Test
    public void test10() {
        // 获取数字的个数、最小值、最大值、总和以及平均值
        List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        IntSummaryStatistics stats = primes.stream().mapToInt((x) -> x).summaryStatistics();
        System.out.println("Highest prime number in List : " + stats.getMax());
        System.out.println("Lowest prime number in List : " + stats.getMin());
        System.out.println("Sum of all prime numbers : " + stats.getSum());
        System.out.println("Average of all prime numbers : " + stats.getAverage());
    }
}