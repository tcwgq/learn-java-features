package com.tcwgq.learn_java.lambda;

import java.util.Arrays;
import java.util.List;

/**
 * lambda表达式
 * 基本语法
 * (parameters)->expression 或者　(parameters)->{statements;}
 * @author admin
 *
 */
public class LambdaTest1 {
	public static void main(String[] args) {
		String[] names = { "zhangsan", "lisi", "wangwu", "zhaoliu", "wangermazi", "hahaha" };
		List<String> list = Arrays.asList(names);
		for (String s : list) {
			System.out.println(s);
		}
//		list.forEach((s) -> System.out.println(s));
//		list.forEach(System.out::println);
		/**
		 * lambda的例子
		 * ()->5，不需要参数，返回值为5
		 * x->2 * x，接收一个参数(数字类型)，并返回其的2倍。
		 * (x, y)-> x-y，接收2个参数(数字)，并返回它们的差值。
		 * (int x, int y)->x+y，接收2个int型的参数，并返回它们的和。
		 * (String s)->System.out.println(s)，接收一个String对象，并在控制台打印，不返回任何值。
		 */
	}
}
