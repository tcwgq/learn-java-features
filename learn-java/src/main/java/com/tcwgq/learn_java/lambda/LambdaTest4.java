package com.tcwgq.learn_java.lambda;

import java.util.ArrayList;
import java.util.List;

public class LambdaTest4 {
	public static void main(String[] args) {
		List<Student> students = new ArrayList<Student>() {
			{
				add(new Student("zhangsan", 23));
				add(new Student("lisi", 24));
				add(new Student("wangwu", 25));
				add(new Student("zhaoliu", 26));
				add(new Student("wangermazi", 27));
				add(new Student("golf", 35));
				add(new Student("white", 27));
			}
		};
		// lambda表达式遍历
		// students.forEach((person) -> System.out.printf("%s-%s \n",
		// person.getName(), person.getAge()));
		// lambda表达式排序
		// List<Student> sortedList = students.stream().sorted((s1, s2) ->
		// (s1.getName().compareTo(s2.getName()))).limit(5).collect(toList());
	}
}
