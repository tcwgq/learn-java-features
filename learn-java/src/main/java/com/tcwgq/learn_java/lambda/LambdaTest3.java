package com.tcwgq.learn_java.lambda;

import java.util.Arrays;
import java.util.Comparator;

public class LambdaTest3 {
	public static void main(String[] args) {
		String[] names = { "zhangsan", "lisi", "wangwu", "zhaoliu", "jack", "lucy", "vans", "baga", "white" };
		Arrays.sort(names, new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return s1.compareTo(s2);
			}
		});
		System.out.println(Arrays.toString(names));
	}
}
