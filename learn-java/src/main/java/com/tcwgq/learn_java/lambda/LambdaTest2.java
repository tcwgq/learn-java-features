package com.tcwgq.learn_java.lambda;

public class LambdaTest2 {
	public static void main(String[] args) {
		// 使用匿名内部类
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Helloworld");
			}
		}).start();
		// 使用lambda表达式
		// new Thread(() -> System.out.println("Helloworld")).start();

		Runnable r1 = new Runnable() {
			@Override
			public void run() {
				System.out.println("r1");
			}
		};
		// 使用lambda表达式
		// Runnable r2 = () -> System.out.println("r2");
		// r1.run();
		// r2.run();
	}
}
