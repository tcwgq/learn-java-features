package com.tcwgq.learn_java.innerclass;

/**
 * 定义在方法中的局部内部类
 * 
 * @author admin
 *
 */
public class Outer1 {
	private int age = 24;

	public void show(final String msg) {
		class Inner1 {
			private int age = 25;

			public void showMsg() {
				System.out.println("Outer age=" + age);
				System.out.println("this.age=" + this.age);
				System.out.println("msg=" + msg);
			}
		}
		new Inner1().showMsg();
	}
}

class InnerClassTest1 {
	public static void main(String[] args) {
		Outer1 outer = new Outer1();
		outer.show("helloworld");
	}
}
