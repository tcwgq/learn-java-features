package com.tcwgq.learn_java.innerclass;

/**
 * 静态内部类
 * 
 * @author admin
 *
 */
public class Outer3 {
	private static int age = 23;

	static class Inner3 {
		public void print() {
			System.out.println(age);
		}
	}

	public void show() {
		System.out.println("I am outer class");
	}
}

class InnerClassTest3 {
	public static void main(String[] args) {
		Outer3.Inner3 inner = new Outer3.Inner3();
		inner.print();
	}
}
