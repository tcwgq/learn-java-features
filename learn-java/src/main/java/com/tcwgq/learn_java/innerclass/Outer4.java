package com.tcwgq.learn_java.innerclass;

public class Outer4 {
	private int age = 23;

	public void show() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("好好学习，天天向上");
			}
		}).start();
		;
		System.out.println(this.age);
	}
}

class InnerClassTest4 {
	public static void main(String[] args) {
		Outer4 outer = new Outer4();
		outer.show();
	}
}
