package com.tcwgq.learn_java.innerclass;

/**
 * 
 * 成员内部类
 * 
 * @author admin
 *
 */
public class Outer {
	private int age = 23;

	class Inner {
		private int age = 24;

		public void print() {
			int age = 25;
			System.out.println("Outer age=" + Outer.this.age);
			System.out.println("Inner age=" + this.age);
			System.out.println("局部变量    age=" + age);
		}
	}

	public void show() {
		/**
		 * 外部类访问内部类的成员，必须先创建内部类的实例
		 */
		Inner inner = new Inner();
		inner.print();
	}

}

class InnerClassTest {
	public static void main(String[] args) {
		/**
		 * 内部类可以直接访问外部类的成员变量
		 */
		Outer outer = new Outer();
		outer.show();
		Outer.Inner inner = outer.new Inner();
		inner.print();
	}
}
