package com.tcwgq.learn_java.innerclass;

/**
 * 定义在作用域内的内部类
 * 
 * @author admin
 *
 */
public class Outer2 {
	private int age = 23;

	public void show(final int a) {
		if (a > 10) {
			class Inner2 {
				private int age = 24;

				public void print() {
					System.out.println("Inner class age=" + age);
					System.out.println("a=" + a);
				}
			}
			new Inner2().print();
		}
	}
}

class InnerClassTest2 {
	public static void main(String[] args) {
		Outer2 outer = new Outer2();
		outer.show(100);
	}
}
