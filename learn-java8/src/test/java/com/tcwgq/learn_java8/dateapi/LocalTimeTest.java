package com.tcwgq.learn_java8.dateapi;

import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.chrono.HijrahDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by wangguangqiang on 2019/10/30 20:48
 */
public class LocalTimeTest {
    @Test
    public void create() {
        LocalTime now = LocalTime.now();
        System.out.println(now);

        LocalTime of = LocalTime.of(20, 49, 59);
        System.out.println(of);
    }

    @Test
    public void parse() {
        LocalTime parse = LocalTime.parse("20:50:21");
        System.out.println(parse);

        LocalTime parse1 = LocalTime.parse("20-20:21", DateTimeFormatter.ofPattern("HH-mm:ss"));
        System.out.println(parse1);

        LocalTime localTime = LocalTime.from(HijrahDate.now());
        System.out.println(localTime);
    }
}
