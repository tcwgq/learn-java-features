package com.tcwgq.learn_java8.dateapi;

import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;

/**
 * Created by wangguangqiang on 2019/10/30 16:55
 */
public class LocalDateTest {
    /**
     * 获取日期
     */
    @Test
    public void test1() {
        // 获取当前日期
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate);//2019-10-30
        int year = localDate.getYear();
        System.out.println(year);//2019
        int i = localDate.get(ChronoField.YEAR);
        System.out.println(i);//2019
        Month month = localDate.getMonth();
        System.out.println(month);//OCTOBER
        int i1 = localDate.get(ChronoField.MONTH_OF_YEAR);
        System.out.println(i1);//10
        int dayOfMonth = localDate.getDayOfMonth();
        System.out.println(dayOfMonth);//30
        int i2 = localDate.get(ChronoField.DAY_OF_MONTH);
        System.out.println(i2);//10
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        System.out.println(dayOfWeek);//WEDNESDAY
        int i3 = localDate.get(ChronoField.DAY_OF_WEEK);
        System.out.println(i3);//3
        // 指定日期
        LocalDate localDate1 = LocalDate.of(2019, 12, 24);
        System.out.println(localDate1);//2019-12-24
        // 获取指定时区的日期
        LocalDate utc = LocalDate.now(ZoneId.of("UTC"));
        System.out.println(utc);//2019-10-30
    }

    /**
     * 解析日期
     */
    @Test
    public void test2() {
        LocalDate parse = LocalDate.parse("2018-12-24");
        System.out.println(parse);

        LocalDate parse1 = LocalDate.parse("2019/10/01", DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        System.out.println(parse1);

        LocalDate parse2 = LocalDate.parse("January 12, 1952", DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT));
        System.out.println(parse2);
    }

    /**
     * 计算时间
     */
    @Test
    public void test3() {
        LocalDate localDate = LocalDate.now();
        LocalDate localDate1 = localDate.plusDays(1);
        System.out.println(localDate1);
        LocalDate localDate2 = localDate.plusMonths(1);
        System.out.println(localDate2);
        LocalDate localDate3 = localDate.plusWeeks(1);
        System.out.println(localDate3);
        // 加1年
        LocalDate plus = localDate.plus(1, ChronoUnit.YEARS);
        System.out.println(plus);
        // 相对应的有减法操作
        LocalDate localDate4 = localDate.plusDays(1);
        System.out.println(localDate4);
        // 计算时间
        LocalDate with = localDate.with(ChronoField.DAY_OF_MONTH, 1);
        System.out.println(with);
    }

    @Test
    public void test4() {
        LocalDate now = LocalDate.now();
        String format = now.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        System.out.println(format);
    }

    @Test
    public void test5() {
        LocalDate l1 = LocalDate.of(2018, 12, 24);
        LocalDate now = LocalDate.now();
        long between = ChronoUnit.DAYS.between(l1, now);
        System.out.println(between);

        long between1 = ChronoUnit.WEEKS.between(l1, now);
        System.out.println(between1);
    }
}
