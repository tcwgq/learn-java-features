package com.tcwgq.learn_java8.function;

import com.tcwgq.learn_java8.model.Company;
import com.tcwgq.learn_java8.model.Employee;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by wangguangqiang on 2019/10/28 16:26
 */
public class OptionalTest {
    @Test
    public void test1() {
        Optional<String> o = Optional.of("Hello");
        if (o.isPresent()) {
            System.out.println(o.get());
        }

        //Optional<String> empty = Optional.empty();
        Optional<String> empty = Optional.of("hahahha");
        empty.ifPresent(System.out::println);
        System.out.println(empty.orElse("world"));

        System.out.println(empty.orElseGet(() -> {
            return "hello world";
        }));

        Optional<String> o1 = Optional.ofNullable("Null");
        o1.ifPresent(System.out::println);
    }

    @Test
    public void test2() {
        Employee e1 = new Employee("e1");
        Employee e2 = new Employee("e2");
        Employee e3 = new Employee("e3");
        List<Employee> employees = Arrays.asList(e1, e2, e3);
        Company company = new Company("iyunbao", employees);
        Optional<Company> optional = Optional.ofNullable(company);
        System.out.println(optional.map(company1 -> company1.getList()).orElse(Collections.emptyList()));
    }

    /**
     * 不要将optional作为方法的参数类型或类的成员变量
     *
     * @param optional
     * @param <T>
     * @return
     */
    private <T> boolean xxx(Optional<T> optional) {
        return optional.isPresent();
    }
}
