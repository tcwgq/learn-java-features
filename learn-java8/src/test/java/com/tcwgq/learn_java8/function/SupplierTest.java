package com.tcwgq.learn_java8.function;

import com.tcwgq.learn_java8.model.User;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Created by wangguangqiang on 2019/9/30 11:17
 */
public class SupplierTest {
    @Test
    public void test1() {
        // 构造函数引用
        Supplier<User> supplier = User::new;
        User user = supplier.get();
        System.out.println(user);
        User user1 = supplier.get();
        System.out.println(user1);
        User user2 = supplier.get();
        System.out.println(user2);
    }

    @Test
    public void test2() {
        Supplier<String> supplier = () -> "Hello world!";
        System.out.println(supplier.get());
    }

    @Test
    public void test3() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
        list.forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.println(integer);
            }
        });
    }

    @Test
    public void test4() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);
        // 方法引用
        list.forEach(System.out::println);
    }
}
