package com.tcwgq.learn_java8.function;

import org.junit.Test;

import java.util.function.Function;

/**
 * Created by wangguangqiang on 2019/10/9 19:27
 */
public class MyFunctionTest {
    @Test
    public void test1() {
        Function<String, String> function = String::toUpperCase;
        System.out.println(function.getClass().getInterfaces()[0]);
    }

    @Test
    public void test2() {
        MyFunctionTest test = new MyFunctionTest();
        // function传递的是行为
        System.out.println(test.compute(1, value -> {
            return 2 * value;
        }));
        System.out.println(test.compute(2, value -> {
            return 2 + value;
        }));
        System.out.println(test.compute(3, value -> {
            return value * value;
        }));

        Function<Integer, String> function = (Integer integer) -> "Hello " + integer;
        System.out.println(test.compute1(2, function));

        // compose，(2*2)+(2*2)
        System.out.println(test.compute2(2, value -> {
            return value + value;
        }, value -> {
            return value * value;
        }));

        // andThen，(2+2)*(2+2)
        System.out.println(test.compute3(2, value -> {
            return value + value;
        }, value -> {
            return value * value;
        }));
    }

    private int compute(int a, Function<Integer, Integer> function) {
        return function.apply(a);
    }

    private String compute1(int a, Function<Integer, String> function) {
        return function.apply(a);
    }

    private int compute2(int a, Function<Integer, Integer> function1, Function<Integer, Integer> function2) {
        return function1.compose(function2).apply(a);
    }

    private int compute3(int a, Function<Integer, Integer> function1, Function<Integer, Integer> function2) {
        return function1.andThen(function2).apply(a);
    }

}
