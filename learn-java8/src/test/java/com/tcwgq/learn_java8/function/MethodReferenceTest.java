package com.tcwgq.learn_java8.function;

import com.tcwgq.learn_java8.model.Student;
import com.tcwgq.learn_java8.model.StudentComparator;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * java8方法引用的4种方式
 * <p>
 * Created by wangguangqiang on 2019/10/29 10:26
 */
public class MethodReferenceTest {
    @Test
    public void test1() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        list.forEach(System.out::println);

        Function<String, String> function = String::toLowerCase;
        System.out.println(function.getClass().getInterfaces()[0]);
    }

    /**
     * 第1种：类类名::静态方法名
     */
    @Test
    public void test2() {
        Student s1 = new Student("zhangSan", 30);
        Student s2 = new Student("liSi", 50);
        Student s3 = new Student("wangwu", 60);
        Student s4 = new Student("zhaoliu", 80);

        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        //Collections.sort(students, (o1, o2) -> o1.getScore() - o2.getScore());// 传统方式
        //students.sort(new Comparator<Student>() {
        //    @Override
        //    public int compare(Student o1, Student o2) {
        //        return Student.compareByScore(o1,o2);
        //    }
        //});

        //students.sort((o1, o2) -> Student.compareByScore(o1,o2));
        // 类名::静态方法名，int compare(T o1, T o2)，接收2个参数，返回1个整型，正好和静态方法声明一致
        //students.sort(Student::compareByScore);
        students.sort(Student::comparedStudentByName);
        students.forEach(student -> System.out.println(student.getScore()));
        students.forEach(student -> System.out.println(student.getName()));
    }

    /**
     * 第2种：引用名【对象名】::实例方法名
     */
    @Test
    public void test3() {
        Student s1 = new Student("zhangSan", 30);
        Student s2 = new Student("liSi", 50);
        Student s3 = new Student("wangwu", 60);
        Student s4 = new Student("zhaoliu", 80);

        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        StudentComparator comparator = new StudentComparator();
        //Collections.sort(students, (o1, o2) -> o1.getScore() - o2.getScore());// 传统方式
        students.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return comparator.compareByScore(o1, o2);
            }
        });

        students.sort((o1, o2) -> comparator.compareByScore(o1, o2));

        students.sort(comparator::compareByScore);

        students.forEach(student -> System.out.println(student.getScore()));
    }

    /**
     * 第3种：类名::实例方法名【最难理解】
     */
    @Test
    public void test4() {
        Student s1 = new Student("zhangSan", 30);
        Student s2 = new Student("liSi", 50);
        Student s3 = new Student("wangwu", 60);
        Student s4 = new Student("zhaoliu", 80);

        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        //Collections.sort(students, (o1, o2) -> o1.getScore() - o2.getScore());// 传统方式
        /**
         * 这个方法一定是由sort()方法接受的Lambda表达式第一个参数来调用的，而如果Lambda表达式有多个参数，则除了第一个参数之外的所有参数都将作为方法的参数传递进去
         */
        students.sort(Student::comparedByScore);

        students.forEach(student -> System.out.println(student.getScore()));

        List<String> stringList = Arrays.asList("beijing", "shanghai", "shenzhen", "guangzhou");
        Collections.sort(stringList, String::compareToIgnoreCase);
        stringList.forEach(System.out::println);
    }

    /**
     * 第4种：构造方法引用，其表现形式为：类名::new
     */
    @Test
    public void test5() {
        System.out.println(getString(String::new));
        System.out.println(getString("hello", String::new));
    }

    private String getString(Supplier<String> supplier) {
        return supplier.get() + "test";
    }

    private String getString(String s, Function<String, String> function) {
        return function.apply(s);
    }
}
