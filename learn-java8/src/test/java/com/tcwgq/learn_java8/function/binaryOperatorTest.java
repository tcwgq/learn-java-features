package com.tcwgq.learn_java8.function;

import org.junit.Test;

import java.util.Comparator;
import java.util.function.BinaryOperator;

/**
 * Created by wangguangqiang on 2019/10/28 14:48
 */
public class binaryOperatorTest {
    @Test
    public void test1() {
        System.out.println(func(1, 2, Integer::sum));
        // 看源码
        BinaryOperator<Integer> bo = BinaryOperator.minBy(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        System.out.println(func(1, 2, bo));

        System.out.println(func(1, 2, BinaryOperator.maxBy(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        })));

        System.out.println(Integer.compare(1, 2));
    }

    private int func(int a, int b, BinaryOperator<Integer> bo) {
        return bo.apply(a, b);
    }
}
