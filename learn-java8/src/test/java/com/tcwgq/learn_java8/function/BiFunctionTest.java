package com.tcwgq.learn_java8.function;

import com.tcwgq.learn_java8.model.Person;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by wangguangqiang on 2019/10/25 16:00
 */
public class BiFunctionTest {
    @Test
    public void test1() {
        BiFunctionTest test = new BiFunctionTest();
        System.out.println(test.compute(1, 2, (a, b) -> {
            return a + b;
        }));
        System.out.println(test.compute1(1, 2, (a, b) -> {
            return a + b;
        }, value -> {
            return value * 2;
        }));
    }

    @Test
    public void test2() {
        Person p1 = new Person(20, "zhangSan");
        Person p2 = new Person(25, "liSi");
        Person p3 = new Person(30, "wangwu");
        List<Person> list = Arrays.asList(p1, p2, p3);
        BiFunctionTest test = new BiFunctionTest();
        List<Person> zhangSan = test.filterPerson("zhangSan", list);
        zhangSan.forEach(person -> System.out.println(person.getName()));

        List<Person> liSi = test.filterPerson1("liSi", list, new BiFunction<String, List<Person>, List<Person>>() {
            @Override
            public List<Person> apply(String s, List<Person> list) {
                return list.parallelStream().filter(person -> person.getName().equals(s)).collect(Collectors.toList());
            }
        });
        liSi.forEach(person -> System.out.println(person.getName()));
    }

    private int compute(int a, int b, BiFunction<Integer, Integer, Integer> function) {
        return function.apply(a, b);
    }

    private int compute1(int a, int b, BiFunction<Integer, Integer, Integer> function, Function<Integer, Integer> function1) {
        return function.andThen(function1).apply(a, b);
    }

    private List<Person> filterPerson(String name, List<Person> list) {
        return list.stream().filter(new Predicate<Person>() {
            @Override
            public boolean test(Person person) {
                return person.getName().equals(name);
            }
        }).collect(Collectors.toList());
    }

    private List<Person> filterPerson1(String name, List<Person> persons, BiFunction<String, List<Person>, List<Person>> biFunction) {
        return biFunction.apply(name, persons);
    }
}
