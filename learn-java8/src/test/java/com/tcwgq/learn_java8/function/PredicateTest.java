package com.tcwgq.learn_java8.function;

import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by wangguangqiang on 2019/10/25 17:23
 */
public class PredicateTest {
    @Test
    public void test1() {
        Predicate<String> predicate = s -> s.length() > 5;
        System.out.println(predicate.test("hello"));
        System.out.println(predicate.test("abc"));
    }

    @Test
    public void test2() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Predicate<Integer> p1 = new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer > 5;
            }
        };
        filter(list, p1);
        Predicate<Integer> p2 = new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return integer % 2 == 0;
            }
        };
        filter(list, p2);

        filter(list, new Predicate<Integer>() {
            @Override
            public boolean test(Integer integer) {
                return false;
            }
        });
    }

    @Test
    public void test3() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Predicate<Integer> p1 = integer -> integer > 5;
        Predicate<Integer> p2 = integer -> integer % 2 == 0;
        Predicate<Integer> p3 = integer -> integer % 2 != 0;
        // and
        filter(list, p1.and(p2));
        // negate
        filter(list, p1.negate());
        // or
        filter(list, p2.or(p3));
        System.out.println(Predicate.isEqual("Hello").test("Hello"));
        System.out.println(Predicate.isEqual("Hello").test("world"));
        System.out.println(Predicate.isEqual(new Date()).test(new Date()));
    }

    private void filter(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer integer : list) {
            if (predicate.test(integer)) {
                System.out.println(integer);
            }
        }
    }
}
