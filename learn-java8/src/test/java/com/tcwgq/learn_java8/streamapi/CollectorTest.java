package com.tcwgq.learn_java8.streamapi;

import org.junit.Test;

import java.util.*;

/**
 * Created by wangguangqiang on 2019/11/5 14:56
 */
public class CollectorTest {
    @Test
    public void test1() {
        List<String> list = Arrays.asList("hello", "world", "java", "hello");
        Set<String> collect = list.stream().collect(new MyCollector<>());
        System.out.println(collect);
    }

    @Test
    public void test2() {
        List<String> list = Arrays.asList("hello", "world", "java", "hello", "a", "b", "c");
        Map<String, String> collect = list.stream().collect(new MyCollector2<>());
        System.out.println(collect);
    }

    @Test
    public void test3() {
        List<String> list = Arrays.asList("hello", "world", "java", "hello", "a", "b", "c", "d", "e");
        Set<String> set = new HashSet<>(list);
        Map<String, String> collect = set.parallelStream().collect(new MyCollector2<>());
        System.out.println(collect);
    }

    @Test
    public void test4() {
        for (int i = 0; i < 100; i++) {
            List<String> list = Arrays.asList("hello", "world", "java", "hello", "a", "b", "c", "d", "e");
            Set<String> set = new HashSet<>(list);
            Map<String, String> collect = set.parallelStream().collect(new MyCollector2<>());
            System.out.println(collect);
        }
    }

    @Test
    public void test5() {
        for (int i = 0; i < 1; i++) {
            List<String> list = Arrays.asList("hello", "world", "java", "hello", "a", "b", "c", "d", "e");
            Set<String> set = new HashSet<>(list);
            Map<String, String> collect = set.parallelStream().collect(new MyCollector2<>());
            System.out.println(collect);
        }
    }

    @Test
    public void test6() {
        List<String> list = Arrays.asList("hello", "world", "java", "hello", "a", "b", "c", "d", "e");
        Set<String> set = new HashSet<>(list);
        Map<String, String> collect = set.stream().parallel().collect(new MyCollector2<>());
        System.out.println(collect);
    }

    @Test
    public void test7() {
        List<String> list = Arrays.asList("hello", "world", "java", "hello", "a", "b", "c", "d", "e");
        Set<String> set = new HashSet<>(list);
        Map<String, String> collect1 = set.stream().sequential().collect(new MyCollector2<>());
        System.out.println(collect1);
    }

    @Test
    public void test8() {
        int i = Runtime.getRuntime().availableProcessors();
        System.out.println(i);
        List<String> list = Arrays.asList("hello", "world", "java", "hello", "a", "b", "c", "d", "e");
        Set<String> set = new HashSet<>(list);
        // 不管调用多少次，以最后1次为准，最后为并行流
        Map<String, String> collect1 = set.stream().sequential().parallel().sequential().parallel().collect(new MyCollector2<>());
        System.out.println(collect1);
    }
}
