package com.tcwgq.learn_java8.streamapi;

import org.junit.Test;

import java.util.IntSummaryStatistics;
import java.util.OptionalInt;
import java.util.stream.Stream;

/**
 * Created by wangguangqiang on 2019/10/30 10:41
 */
public class StreamTrapTest {
    @Test
    public void test1() {
        // 先构造6个奇数
        Stream<Integer> stream = Stream.iterate(1, value -> value + 2).limit(6);
        // 寻找大于2的，然后乘以2，然后忽略前2个，然后取后2个，最后求和
        int sum = stream.filter(value -> value > 2).mapToInt(value -> value << 1).skip(2).limit(2).sum();
        System.out.println(sum);
    }

    @Test
    public void test2() {
        // 先构造6个奇数
        Stream<Integer> stream = Stream.iterate(1, value -> value + 2).limit(6);
        // 寻找大于2的，然后乘以2，然后忽略前2个，然后取后2个，最后求和
        OptionalInt min = stream.filter(value -> value > 200).mapToInt(value -> value << 1).skip(2).limit(2).min();
        //min.getAsInt();
        min.ifPresent(System.out::println);
    }

    @Test
    public void test3() {
        // 先构造6个奇数
        Stream<Integer> stream = Stream.iterate(1, value -> value + 2).limit(6);
        // 寻找大于2的，然后乘以2，然后忽略前2个，然后取后2个，最后求和
        IntSummaryStatistics intSummaryStatistics = stream.filter(value -> value > 2).mapToInt(value -> value << 1).skip(2).limit(2).summaryStatistics();
        System.out.println(intSummaryStatistics.getMax());
        System.out.println(intSummaryStatistics.getMin());
        System.out.println(intSummaryStatistics.getSum());
        System.out.println(intSummaryStatistics.getAverage());
        System.out.println(intSummaryStatistics.getCount());
    }

    @Test
    public void test4() {
        // 流只能被使用过一次
        Stream<Integer> stream = Stream.iterate(1, value -> value + 2).limit(6);
        System.out.println(stream);
        System.out.println(stream.filter(value -> value > 2));
        System.out.println(stream.distinct());
    }

    @Test
    public void test5() {
        // 流只能被使用过一次
        Stream<Integer> stream = Stream.iterate(1, value -> value + 2).limit(6);
        System.out.println(stream);
        Stream<Integer> integerStream = stream.filter(value -> value > 2);
        System.out.println(integerStream);
        Stream<Integer> distinct = integerStream.distinct();
        System.out.println(distinct);
    }

    @Test
    public void test6() {
        // 流只能被使用过一次
        Stream<Integer> stream = Stream.iterate(1, value -> value + 2).limit(6);
        System.out.println(stream);
        Stream<Integer> integerStream = stream.filter(value -> value > 2);
        System.out.println(integerStream);
        Stream<Integer> distinct = integerStream.distinct();
        System.out.println(distinct);
    }

    @Test
    public void test7() {
        // 流只能被使用过一次
        //Stream.iterate(0, value -> (value + 1) % 2).distinct().limit(6).forEach(System.out::println); // 程序不退出
        // 注意中间操作的顺序
        Stream.iterate(0, value -> (value + 1) % 2).limit(6).distinct().forEach(System.out::println);
    }
}
