package com.tcwgq.learn_java8.streamapi;

import com.tcwgq.learn_java8.model.Student;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by wangguangqiang on 2019/11/4 15:22
 */
public class StreamSourceTest {
    @Test
    public void test1() {
        Student s1 = new Student("1", 200, 12);
        Student s2 = new Student("2", 180, 32);
        Student s3 = new Student("3", 160, 40);
        Student s4 = new Student("2", 160, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        // 分区，是一种特殊的分组，只能分为2组
        List<Student> collect = students.stream().collect(Collectors.toList());
        System.out.println(collect);
        System.out.println(students.stream().count());
        System.out.println(students.stream().collect(Collectors.counting()));
    }

    @Test
    public void test2() {
        Student s1 = new Student("1", 200, 12);
        Student s2 = new Student("2", 180, 32);
        Student s3 = new Student("3", 160, 40);
        Student s4 = new Student("4", 160, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        // 分区，是一种特殊的分组，只能分为2组
        Function<Student, String> function = Student::getName;
        TreeMap<Integer, Set<String>> collect = students.stream().
                collect(Collectors.groupingBy(Student::getScore, TreeMap::new, Collectors.mapping(Student::getName, Collectors.toSet())));
        System.out.println(collect);
    }
}
