package com.tcwgq.learn_java8.streamapi;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

/**
 * Created by wangguangqiang on 2019/10/30 14:52
 */
public class StreamConcurrentTest {
    @Test
    public void test1() {
        List<String> list = new ArrayList<>(5000000);
        for (int i = 0; i < 5000000; i++) {
            list.add(UUID.randomUUID().toString());
        }
        long start = System.nanoTime();
        long count = list.stream().sorted().count();
        System.out.println(count);
        long end = System.nanoTime();
        System.out.println("耗时: " + TimeUnit.NANOSECONDS.toMillis(end - start));
    }

    @Test
    public void test2() {
        List<String> list = new ArrayList<>(5000000);
        for (int i = 0; i < 5000000; i++) {
            list.add(UUID.randomUUID().toString());
        }
        long start = System.nanoTime();
        long count = list.parallelStream().sorted().count();
        System.out.println(count);
        long end = System.nanoTime();
        System.out.println("耗时: " + TimeUnit.NANOSECONDS.toMillis(end - start));
    }

    @Test
    public void test3() {
        List<String> list = Arrays.asList("hello1", "world", "hello world");
        list.stream().mapToInt(new ToIntFunction<String>() {
            @Override
            public int applyAsInt(String value) {
                System.out.println(value);
                return value.length();
            }
        }).filter(value -> value == 5).findFirst().ifPresent(System.out::println);
    }

    @Test
    public void test4() {
        List<String> list = Arrays.asList("hello world", "welcome world", "hello welcome", "hello welcome world");
        // 竟然为4个数组
        list.stream().map(value -> value.split(" ")).distinct().forEach(System.out::println);
        List<String[]> result = list.stream().map(value -> value.split(" ")).distinct().collect(Collectors.toList());
        result.forEach(value -> Arrays.asList(value).forEach(System.out::println));
        // 多个stream合并为一个
        list.stream().map(value -> value.split(" ")).flatMap(Arrays::stream).distinct().forEach(System.out::println);
    }
}
