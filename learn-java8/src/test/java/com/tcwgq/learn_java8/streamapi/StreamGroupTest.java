package com.tcwgq.learn_java8.streamapi;

import com.tcwgq.learn_java8.model.Student;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by wangguangqiang on 2019/11/4 14:16
 */
public class StreamGroupTest {
    @Test
    public void test1() {
        //  list1与list2的组合
        List<String> list1 = Arrays.asList("Hi", "Hello", "你好");
        List<String> list2 = Arrays.asList("zhangsan", "lisi", "wangwu", "zhaoliu");
        List<String> collect = list1.stream().flatMap(item -> list2.stream().map(item2 -> item + " " + item2)).collect(Collectors.toList());
        collect.forEach(System.out::println);
    }

    @Test
    public void test2() {
        Student s1 = new Student("1", 200, 12);
        Student s2 = new Student("2", 180, 32);
        Student s3 = new Student("3", 160, 40);
        Student s4 = new Student("2", 160, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        // 根据姓名分组，select * from student group by name;
        Map<String, List<Student>> collect = students.stream().collect(Collectors.groupingBy(Student::getName));
        System.out.println(collect);
        // 根据分数分组
        Map<Integer, List<Student>> collect1 = students.stream().collect(Collectors.groupingBy(Student::getScore));
        System.out.println(collect1);
        // select name, count(*) from student group by name;
        Map<String, Long> collect2 = students.stream().collect(Collectors.groupingBy(Student::getName, Collectors.counting()));
        Map<String, Long> collect22 = students.stream().collect(Collectors.groupingBy(Student::getName, Collectors.reducing(0L, e -> 1L, Long::sum)));
        System.out.println(collect2);
        System.out.println(collect22);

        // select name, average(score) from student group by name;
        Map<String, Double> collect3 = students.stream().collect(Collectors.groupingBy(Student::getName, Collectors.averagingDouble(Student::getScore)));
        System.out.println(collect3);
    }

    @Test
    public void test3() {
        Student s1 = new Student("1", 200, 12);
        Student s2 = new Student("2", 180, 32);
        Student s3 = new Student("3", 160, 40);
        Student s4 = new Student("2", 160, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        // 分区，是一种特殊的分组，只能分为2组
        Map<Boolean, List<Student>> collect = students.stream().collect(Collectors.partitioningBy(student -> student.getScore() <= 180));
        System.out.println(collect);
    }
}
