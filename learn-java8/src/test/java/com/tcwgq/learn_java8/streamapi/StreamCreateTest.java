package com.tcwgq.learn_java8.streamapi;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * stream的4种创建方式
 * <p>
 * Created by wangguangqiang on 2019/10/29 14:22
 */
public class StreamCreateTest {
    @Test
    public void test1() {
        Stream<String> stringStream = Stream.of("hello", "world", "java");

        String[] strings = new String[]{"hello", "world", "java"};
        Stream<String> stringStream1 = Stream.of(strings);

        Stream<String> stringStream2 = Arrays.stream(new String[]{"hello", "world"});

        List<String> stringList = Arrays.asList(strings);
        Stream<String> stream = stringList.stream();

        Stream.Builder<String> builder = Stream.builder();
        Stream<String> build = builder.add("hello").add("world").add("java").build();
    }

    @Test
    public void test2() {
        IntStream.of(new int[]{1, 2, 3}).forEach(System.out::println);
        // 不包含结尾
        IntStream.range(1, 10).forEach(System.out::println);
        // 包含结尾
        IntStream.rangeClosed(2, 8).forEach(System.out::println);
    }

    @Test
    public void test3() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        Integer reduce = list.stream().map(a -> a * 2).reduce(0, Integer::sum);
        System.out.println(reduce);
    }
}
