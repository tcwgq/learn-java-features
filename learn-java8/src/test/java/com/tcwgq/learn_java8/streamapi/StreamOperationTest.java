package com.tcwgq.learn_java8.streamapi;

import org.junit.Test;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by wangguangqiang on 2019/10/29 15:25
 */
public class StreamOperationTest {
    @Test
    public void test1() {
        Stream<String> stream = Stream.of("hello", "world", "java");
        //String[] strings = stream.toArray(new IntFunction<String[]>() {
        //    @Override
        //    public String[] apply(int value) {
        //        return new String[value];
        //    }
        //});
        //String[] strings1 = stream.toArray(value -> new String[value]);
        // stream转数组
        String[] strings2 = stream.toArray(String[]::new);
        Arrays.asList(strings2).forEach(System.out::println);
    }

    @Test
    public void test2() {
        Stream<String> stream = Stream.of("hello", "world", "java");
        String collect = stream.collect(Collectors.joining());
        Collection<String> collect1 = stream.collect(Collectors.toCollection(ArrayList::new));
        List<String> collect2 = stream.collect(Collectors.toList());
        Set<String> collect3 = stream.collect(Collectors.toSet());
    }

    @Test
    public void test3() {
        Stream<String> stream = Stream.of("hello", "world", "java");
        //List<String> collect = stream.collect(new Supplier<List<String>>() {
        //    @Override
        //    public List<String> get() {
        //        return new ArrayList<>();
        //    }
        //}, new BiConsumer<List<String>, String>() {
        //    @Override
        //    public void accept(List<String> strings, String s) {
        //        strings.add(s);
        //    }
        //}, new BiConsumer<List<String>, List<String>>() {
        //    @Override
        //    public void accept(List<String> strings, List<String> strings2) {
        //        strings.addAll(strings2);
        //    }
        //});

        List<String> collect = stream.collect(ArrayList::new, List::add, (BiConsumer<List<String>, List<String>>) List::addAll);
        collect.forEach(System.out::println);
    }

    @Test
    public void test4() {
        // stream转集合
        List<String> collect = Stream.of("hello", "world", "java").collect(Collectors.toCollection(ArrayList::new));
        collect.forEach(System.out::println);
        TreeSet<String> collect1 = Stream.of("hello", "world", "java").collect(Collectors.toCollection(TreeSet::new));
        collect1.forEach(System.out::println);
        // 字符拼接
        //String s = String.join("", "hello", "world", "java").toString();
        String s = Stream.of("hello", "world", "java").collect(Collectors.joining());
        System.out.println(s);
        // map操作
        Stream.of("hello", "world", "java").map(String::toUpperCase).forEach(System.out::println);
        Stream.of(1, 2, 3, 4, 5).map(value -> value * value).forEach(System.out::println);

        Stream<List<Integer>> listStream = Stream.of(Arrays.asList(1, 2), Arrays.asList(4, 5, 6), Arrays.asList(7, 8, 9, 10));
        Stream<Integer> objectStream = listStream.flatMap(new Function<List<Integer>, Stream<Integer>>() {
            @Override
            public Stream<Integer> apply(List<Integer> integers) {
                return integers.stream();
            }
        });
        objectStream.map(value -> value * value).forEach(System.out::println);
    }

    @Test
    public void test5() {
        Stream<String> generate = Stream.generate(() -> UUID.randomUUID().toString());

        //System.out.println(generate.findFirst());
        //System.out.println(generate.findFirst().get());
        generate.findFirst().ifPresent(System.out::println);

        Stream<String> generate1 = Stream.empty();
        generate1.findFirst().ifPresent(System.out::println);
    }

    @Test
    public void test6() {
        // 无限制
        //Stream.iterate(2, value -> value + 2).forEach(System.out::println);
        // 限制
        Stream.iterate(2, value -> value + 2).limit(10).forEach(System.out::println);
    }

    @Test
    public void test7() {
        Stream.of("hello", "world", "java").map(s -> {
            char[] chars = s.toCharArray();
            chars[0] -= 32;
            return String.valueOf(chars);
        }).peek(System.out::println).filter(v -> v.length() > 2).forEach(System.out::println);
    }

}
