package com.tcwgq.learn_java8.streamapi;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by wangguangqiang on 2019/11/5 10:48
 */
public class ComparatorTest {
    @Test
    public void test1() {
        //Student s1 = new Student("1", 200, 12);
        //Student s2 = new Student("2", 180, 32);
        //Student s3 = new Student("3", 160, 40);
        //Student s4 = new Student("2", 160, 40);
        //List<Student> students = Arrays.asList(s1, s2, s3, s4);

        List<String> stringList = Arrays.asList("hello", "world", "java", "apple");
        Collections.sort(stringList);
        System.out.println(stringList);
        // 长度升序排序
        Collections.sort(stringList, (s1, s2) -> {
            return s1.length() - s2.length();
        });
        System.out.println(stringList);
        // 长度降序排序
        Collections.sort(stringList, (s1, s2) -> {
            return s2.length() - s1.length();
        });
        System.out.println(stringList);

        // 长度排序，改为使用特殊方法
        Collections.sort(stringList, Comparator.comparingInt(String::length));
        System.out.println(stringList);
    }

    @Test
    public void test2() {
        List<String> stringList = Arrays.asList("hello", "world", "java", "apple");
        // 长度降序排序，改为使用特殊方法
        Collections.sort(stringList, Comparator.comparingInt(String::length).reversed());
        System.out.println(stringList);
    }

    /**
     * 类型推断特例
     */
    @Test
    public void test3() {
        List<String> stringList = Arrays.asList("hello", "world", "java", "apple");
        // 长度降序排序，改为使用特殊方法
        // 泛型无法推断，此时lambda表达式需要显示指出参数类型
        Collections.sort(stringList, Comparator.comparingInt((String value) -> value.length()).reversed());
        System.out.println(stringList);
    }

    @Test
    public void test4() {
        // 使用List新增的sort方法
        List<String> list = Arrays.asList("hello", "world", "java", "apple");
        list.sort(Comparator.comparingInt(String::length));
        System.out.println(list);
        list.sort(Comparator.comparingInt(String::length).reversed());
        System.out.println(list);
        // 先按长度排序，长度相同，则按字母表排序
        list.sort(Comparator.comparingInt(String::length).thenComparing(String.CASE_INSENSITIVE_ORDER));
        System.out.println(list);
        // 同上
        list.sort(Comparator.comparingInt(String::length).thenComparing(String::compareToIgnoreCase));
        System.out.println(list);
        // 同上
        list.sort(Comparator.comparingInt(String::length).thenComparing(Comparator.comparing(String::toLowerCase)));
        System.out.println(list);
        // 先根据长度排序，然后长度相同的根据字典逆序排序
        list.sort(Comparator.comparingInt(String::length).thenComparing(Comparator.comparing(String::toLowerCase, Comparator.reverseOrder())));
        System.out.println(list);
        // 只有前一个比较返回的结果为0，后一个比较才能执行
        list.sort(Comparator.comparingInt(String::length).thenComparing(Comparator.comparing(String::toLowerCase, Comparator.reverseOrder())).thenComparing(Comparator.reverseOrder()));
        System.out.println(list);
    }
}
