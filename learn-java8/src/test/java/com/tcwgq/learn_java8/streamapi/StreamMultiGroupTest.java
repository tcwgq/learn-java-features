package com.tcwgq.learn_java8.streamapi;

import com.tcwgq.learn_java8.model.Student;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by wangguangqiang on 2019/11/4 17:30
 */
public class StreamMultiGroupTest {
    @Test
    public void test1() {
        Student s1 = new Student("1", 80, 12);
        Student s2 = new Student("2", 90, 32);
        Student s3 = new Student("3", 99, 40);
        Student s4 = new Student("2", 60, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        // 分区，是一种特殊的分组，只能分为2组
        List<Student> collect = students.stream().collect(Collectors.toList());
        System.out.println(collect);
        // 流中元素个数
        System.out.println(students.stream().count());
        System.out.println(students.stream().collect(Collectors.counting()));
        // 找出集合中分数最小的元素
        students.stream().collect(Collectors.minBy(Comparator.comparingInt(Student::getScore))).ifPresent(System.out::println);
        students.stream().collect(Collectors.maxBy(Comparator.comparingInt(Student::getScore))).ifPresent(System.out::println);
        Double collect1 = students.stream().collect(Collectors.averagingInt(Student::getScore));
        System.out.println(collect1);
        Integer collect2 = students.stream().collect(Collectors.summingInt(Student::getScore));
        System.out.println(collect2);
        // 概要统计
        IntSummaryStatistics collect3 = students.stream().collect(Collectors.summarizingInt(Student::getScore));
        System.out.println(collect3);
    }

    /**
     * 连接操作
     */
    @Test
    public void test2() {
        Student s1 = new Student("hello", 80, 12);
        Student s2 = new Student("world", 90, 32);
        Student s3 = new Student("welcome", 99, 40);
        Student s4 = new Student("java", 60, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4);
        String collect = students.stream().map(Student::getName).collect(Collectors.joining());
        System.out.println(collect);
        String collect1 = students.stream().map(Student::getName).collect(Collectors.joining(","));
        System.out.println(collect1);
        String collect2 = students.stream().map(Student::getName).collect(Collectors.joining(",", "[", "]"));
        System.out.println(collect2);
    }

    /**
     * 多级分组
     */
    @Test
    public void test3() {
        Student s1 = new Student("hello", 80, 12);
        Student s2 = new Student("world", 90, 32);
        Student s3 = new Student("welcome", 99, 40);
        Student s4 = new Student("java", 60, 40);
        Student s5 = new Student("java", 60, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4, s5);
        Map<Integer, Map<String, List<Student>>> collect = students.stream().collect(Collectors.groupingBy(Student::getScore, Collectors.groupingBy(Student::getName)));
        System.out.println(collect);
    }

    @Test
    public void test4() {
        Student s1 = new Student("hello", 80, 12);
        Student s2 = new Student("world", 90, 32);
        Student s3 = new Student("welcome", 99, 40);
        Student s4 = new Student("java", 60, 40);
        Student s5 = new Student("java", 60, 40);
        List<Student> students = Arrays.asList(s1, s2, s3, s4, s5);
        Map<Boolean, Map<Boolean, List<Student>>> collect = students.stream().
                collect(Collectors.partitioningBy(student -> student.getScore() > 80,
                        Collectors.partitioningBy(student -> student.getName().length() > 10)));
        System.out.println(collect);
        Map<Boolean, Long> collect1 = students.stream().
                collect(Collectors.partitioningBy(student -> student.getScore() > 80,
                        Collectors.counting()));
        System.out.println(collect1);
        Map<String, Optional<Student>> collect2 = students.stream().
                collect(Collectors.groupingBy(Student::getName,
                        Collectors.minBy(Comparator.comparingInt(Student::getScore))));
        System.out.println(collect2);

        Map<String, Student> collect3 = students.stream().
                collect(Collectors.groupingBy(Student::getName,
                        Collectors.collectingAndThen(Collectors.minBy(Comparator.comparingInt(Student::getScore)), Optional::get)));
        System.out.println(collect3);
    }
}
