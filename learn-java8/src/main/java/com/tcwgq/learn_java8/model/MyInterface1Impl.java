package com.tcwgq.learn_java8.model;

import com.tcwgq.learn_java8.interfaces.MyInterface1;

/**
 * Created by wangguangqiang on 2019/10/29 11:28
 */
public class MyInterface1Impl implements MyInterface1 {
    @Override
    public void myMethod() {
        System.out.println("MyInterface1Impl");
    }
}
