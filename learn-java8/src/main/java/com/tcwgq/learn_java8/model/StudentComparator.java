package com.tcwgq.learn_java8.model;

/**
 * Created by wangguangqiang on 2019/10/29 11:00
 */
public class StudentComparator {
    public int compareByName(Student s1, Student s2) {
        return s1.getName().compareToIgnoreCase(s2.getName());
    }

    public int compareByScore(Student s1, Student s2) {
        return s1.getScore() - s2.getScore();
    }
}
