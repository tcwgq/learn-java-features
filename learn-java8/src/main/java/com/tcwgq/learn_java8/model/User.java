package com.tcwgq.learn_java8.model;

import lombok.Data;

/**
 * Created by wangguangqiang on 2019/9/30 11:16
 */
@Data
public class User {
    private String name;
    private Integer age;
}
