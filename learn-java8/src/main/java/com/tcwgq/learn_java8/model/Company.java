package com.tcwgq.learn_java8.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by wangguangqiang on 2019/10/28 17:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Company {
    private String name;
    private List<Employee> list;
}
