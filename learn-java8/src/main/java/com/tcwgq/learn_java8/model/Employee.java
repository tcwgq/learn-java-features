package com.tcwgq.learn_java8.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by wangguangqiang on 2019/10/28 17:32
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private String name;
}
