package com.tcwgq.learn_java8.model;

import lombok.Data;

/**
 * Created by wangguangqiang on 2019/10/29 10:34
 */
@Data
public class Student {
    private String name;
    private int score;
    private int age;

    public Student() {
    }

    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public Student(String name, int score, int age) {
        this.name = name;
        this.score = score;
        this.age = age;
    }

    public static int comparedStudentByName(Student s1, Student s2) {
        return s1.name.compareToIgnoreCase(s2.name);
    }

    public static int comparedStudentByScore(Student s1, Student s2) {
        return s1.score - s2.score;
    }

    public int comparedByName(Student s2) {
        return this.name.compareToIgnoreCase(s2.name);
    }

    public int comparedByScore(Student s2) {
        return this.score - s2.score;
    }
}
