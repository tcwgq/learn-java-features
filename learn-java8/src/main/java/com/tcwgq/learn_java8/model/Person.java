package com.tcwgq.learn_java8.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by wangguangqiang on 2019/10/25 16:46
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private int age;
    private String name;
}
