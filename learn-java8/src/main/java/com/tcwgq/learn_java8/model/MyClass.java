package com.tcwgq.learn_java8.model;

import com.tcwgq.learn_java8.interfaces.MyInterface1;
import com.tcwgq.learn_java8.interfaces.MyInterface2;

/**
 * 默认方法
 * Created by wangguangqiang on 2019/10/29 11:25
 */
public class MyClass implements MyInterface1, MyInterface2 {
    @Override
    public void myMethod() {
        System.out.println("MyClass");
        // 接口默认方法引用
        MyInterface1.super.myMethod();
    }

    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        myClass.myMethod();
    }
}
