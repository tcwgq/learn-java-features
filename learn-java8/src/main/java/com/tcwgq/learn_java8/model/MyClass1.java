package com.tcwgq.learn_java8.model;

import com.tcwgq.learn_java8.interfaces.MyInterface2;

/**
 * 实现类的优先级高于接口
 * Created by wangguangqiang on 2019/10/29 11:29
 */
public class MyClass1 extends MyInterface1Impl implements MyInterface2 {
    public static void main(String[] args) {
        MyClass1 myClass = new MyClass1();
        myClass.myMethod();
    }
}
