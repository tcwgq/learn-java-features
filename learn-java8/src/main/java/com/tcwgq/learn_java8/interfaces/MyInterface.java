package com.tcwgq.learn_java8.interfaces;

/**
 * Created by wangguangqiang on 2019/9/30 14:52
 */
@FunctionalInterface
public interface MyInterface {
    void test();

    //void test1();

    //@Override
    String toString();
}
