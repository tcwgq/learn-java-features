package com.tcwgq.learn_java8.interfaces;

/**
 * Created by wangguangqiang on 2019/10/29 11:24
 */
public interface MyInterface2 {
    default void myMethod() {
        System.out.println("MyInterface2");
    }
}
