package com.tcwgq.learn_java8.interfaces;

import java.util.Objects;

public interface MyFunction<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t);

    /**
     * Returns a composed function that first applies the {@code before}
     * function to its input, and then applies this function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param <V>    the type of input to the {@code before} function, and to the
     *               composed function
     * @param before the function to apply before this function is applied
     * @return a composed function that first applies the {@code before}
     * function and then applies this function
     * @throws NullPointerException if before is null
     * @see #andThen(MyFunction)
     */
    default <V> MyFunction<V, R> compose(MyFunction<? super V, ? extends T> before) {
        Objects.requireNonNull(before);
        //    return     new Function<V, R>() {
        //    @Override
        //    public R apply(V v) {
        //        T apply = before.apply(v);
        //        return apply1;
        //    }
        //};

        //Function<V, R> function = new Function<V, R>() {
        //    @Override
        //    public R apply(V v) {
        //        return Function.this.apply(before.apply(v));
        //    }
        //};

        //return function;
        return new MyFunction<V, R>() {
            @Override
            public R apply(V v) {
                T apply = before.apply(v);
                // 这里需要调用外层的apply方法，接口是这么调的
                return MyFunction.this.apply(apply);
            }
        };
    }

    /**
     * Returns a composed function that first applies this function to
     * its input, and then applies the {@code after} function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param <V>   the type of output of the {@code after} function, and of the
     *              composed function
     * @param after the function to apply after this function is applied
     * @return a composed function that first applies this function and then
     * applies the {@code after} function
     * @throws NullPointerException if after is null
     * @see #compose(MyFunction)
     */
    default <V> MyFunction<T, V> andThen(MyFunction<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }

    /**
     * Returns a function that always returns its input argument.
     *
     * @param <T> the type of the input and output objects to the function
     * @return a function that always returns its input argument
     */
    static <T> MyFunction<T, T> identity() {
        MyFunction<T, T> ttMyFunction = new MyFunction<T, T>() {
            @Override
            public T apply(T t) {
                return t;
            }
        };

        return ttMyFunction;
    }
}