package com.tcwgq.learn_java8.streamapi;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * Created by wangguangqiang on 2019/11/5 16:04
 */
public class MyCollector2<T> implements Collector<T, Set<T>, Map<T, T>> {
    @Override
    public Supplier<Set<T>> supplier() {
        System.out.println("supplier...");
        return () -> {
            System.out.println("创建...");
            return new HashSet<>();
        };
    }

    @Override
    public BiConsumer<Set<T>, T> accumulator() {
        System.out.println("accumulator...");
        return (set, item) -> {
            System.out.println("accumulator: " + set + " ," + Thread.currentThread().getName());
            //System.out.println("accumulator: " + " ," + Thread.currentThread().getName());
            set.add(item);
        };
    }

    @Override
    public BinaryOperator<Set<T>> combiner() {
        System.out.println("combiner...");
        return (a, b) -> {
            System.out.println("set1: " + a + " set2: " + b);
            a.addAll(b);
            return a;
        };
    }

    @Override
    public Function<Set<T>, Map<T, T>> finisher() {
        System.out.println("finisher...");
        return set -> {
            Map<T, T> map = new HashMap<>();
            set.forEach(item -> map.put(item, item));
            return map;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        System.out.println("characteristics...");
        return Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.UNORDERED/*,
                Characteristics.CONCURRENT*/));
    }
}
