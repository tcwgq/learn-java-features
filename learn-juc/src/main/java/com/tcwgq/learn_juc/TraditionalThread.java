package com.tcwgq.learn_juc;

public class TraditionalThread {
	public static void main(String[] args) {
		Thread t1 = new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(500l);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("1:" + Thread.currentThread().getName());
					System.out.println("2:" + this.getName());
				}
			}
		};
		t1.start();
		System.out.println(Thread.currentThread().getName());

		Thread t2 = new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(500l);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("1:" + Thread.currentThread().getName());
					System.out.println("2:" + Thread.currentThread().getName());
				}
			}
		});

		t2.start();

		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(500l);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("runnable:" + Thread.currentThread().getName());
				}
			}
		}) {
			public void run() {
				while (true) {
					try {
						Thread.sleep(500l);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("thread:" + Thread.currentThread().getName());
				}
			}
		}.start();
	}

}
