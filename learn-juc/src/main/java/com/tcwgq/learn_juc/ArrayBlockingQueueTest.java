package com.tcwgq.learn_juc;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

public class ArrayBlockingQueueTest {
	public static void main(String[] args) {
		ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<>(3);
		for (int i = 0; i < 3; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					while (true) {
						try {
							int data = new Random().nextInt(10);
							System.out.println(Thread.currentThread().getName() + "正在放入数据");
							Thread.sleep(1000);
							queue.put(data);
							System.out.println("队列目前有" + queue.size() + "个数据");
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();
			new Thread(new Runnable() {
				@Override
				public void run() {
					while (true) {
						try {
							System.out.println(Thread.currentThread().getName() + "正在取出数据");
							Thread.sleep(10);
							queue.take();
							System.out.println("队列目前有" + queue.size() + "个数据");
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();
		}
	}
}
