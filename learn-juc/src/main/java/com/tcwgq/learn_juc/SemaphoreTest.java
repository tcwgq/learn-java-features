package com.tcwgq.learn_juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreTest {
	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		Semaphore s = new Semaphore(3);
		for (int i = 0; i < 10; i++) {
			Runnable r = new Runnable() {
				@Override
				public void run() {
					try {
						s.acquire();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out
							.println(Thread.currentThread().getName() + " 进入，当前已有" + (3 - s.availablePermits()) + "并发");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					s.release();
					System.out.println(Thread.currentThread().getName() + " 离开，当前已有" + s.availablePermits() + "许可");
				}
			};
			service.execute(r);
		}

	}
}
