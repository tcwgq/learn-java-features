package com.tcwgq.learn_juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 第一题：现有的程序代码模拟产生了16个日志对象，并且需要运行16秒才能打印完这些日志，请在程序中增加4个线程去调用parseLog()方法来分头打印这16个日志对象，程序只需要运行4秒即可打印完这些日志对象。原始代码如下：
 * 
 * @author lenovo
 *
 */
public class Test1 {
	public static void main(String[] args) {
		// 容量为1或16都可以
		BlockingQueue<String> queue = new ArrayBlockingQueue<>(16);
		System.out.println("begin:" + (System.currentTimeMillis() / 1000));
		for (int i = 0; i < 4; i++) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					while (true) {
						try {
							String log = queue.take();
							parseLog(log);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();
		}
		for (int i = 0; i < 16; i++) {// 这行代码不能改动
			final String log = (i + 1) + "";// 这行代码不能改动
			{
				try {
					queue.put(log);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// Test1.parseLog(log);
			}
		}
	}

	public static void parseLog(String log) {// 这个方法不能改动
		System.out.println(log + ":" + (System.currentTimeMillis() / 1000));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
