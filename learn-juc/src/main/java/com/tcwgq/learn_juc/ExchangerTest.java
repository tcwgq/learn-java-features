package com.tcwgq.learn_juc;

import java.util.Random;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExchangerTest {
	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		final Exchanger<String> exchanger = new Exchanger<>();
		service.execute(new Runnable() {
			@Override
			public void run() {
				String data1 = "hello";
				System.out.println("线程" + Thread.currentThread().getName() + "正在把数据" + data1 + "换出去");
				try {
					Thread.sleep(new Random().nextInt(5) * 1000);
					String exchange = exchanger.exchange(data1);
					System.out.println("线程" + Thread.currentThread().getName() + "换回的数据为" + exchange);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		service.execute(new Runnable() {
			@Override
			public void run() {
				String data2 = "world";
				System.out.println("线程" + Thread.currentThread().getName() + "正在把数据" + data2 + "换出去");
				try {
					Thread.sleep(new Random().nextInt(5) * 1000);
					String exchange = exchanger.exchange(data2);
					System.out.println("线程" + Thread.currentThread().getName() + "换回的数据为" + exchange);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

	}
}
