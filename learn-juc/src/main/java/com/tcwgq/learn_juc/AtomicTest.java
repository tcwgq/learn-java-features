package com.tcwgq.learn_juc;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

import org.junit.Test;

public class AtomicTest {
	@Test
	public void test1() {
		AtomicInteger ai = new AtomicInteger();
		ai.incrementAndGet();
		System.out.println(ai.get());
		ai.decrementAndGet();
		System.out.println(ai.get());
	}

	@Test
	public void test2() {
		AtomicIntegerArray arr = new AtomicIntegerArray(5);
		for (int i = 0; i < 5; i++) {
			arr.set(i, i);
		}
		for (int j = 0; j < arr.length(); j++) {
			System.out.println(arr.get(j));
		}
	}

	@Test
	public void test3() {
		AtomicIntegerFieldUpdater<User> updater = AtomicIntegerFieldUpdater.newUpdater(User.class, "age");
		User user = new User();
		user.setAge(23);
		System.out.println(updater.incrementAndGet(user));
	}
}
