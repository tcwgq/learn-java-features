package com.tcwgq.learn_juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreeConditionCommunication {
	public static void main(String[] args) {
		final Business bu = new Business();
		new Thread(new Runnable() {
			public void run() {
				for (int i = 1; i <= 50; i++) {
					bu.sub(i);
				}
			}
		}).start();

		for (int i = 1; i <= 50; i++) {
			bu.main(i);
		}
	}

	static class Business {
		private boolean shouldSub = true;
		private Lock lock = new ReentrantLock();
		Condition condition = lock.newCondition();

		public void sub(int i) {
			lock.lock();
			try {
				while (!shouldSub) {
					try {
						// this.wait();
						condition.await();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				for (int j = 1; j <= 10; j++) {
					System.out.println("sub thread sequece of " + j + ", loop of " + i);
				}
				shouldSub = false;
				// this.notify();
				condition.signal();
			} finally {
				lock.unlock();
			}

		}

		public void main(int i) {
			lock.lock();
			try {
				while (shouldSub) {
					try {
						// this.wait();
						condition.await();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				for (int j = 1; j <= 100; j++) {
					System.out.println("main thread sequece of " + j + ", loop of " + i);
				}
				shouldSub = true;
				// this.notify();
				condition.signal();
			} finally {
				lock.unlock();
			}
		}
	}

}