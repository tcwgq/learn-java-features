package com.tcwgq.learn_juc;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CyclicBarrierTest {
	public static void main(String[] args) {
		ExecutorService service = Executors.newCachedThreadPool();
		final CyclicBarrier cb = new CyclicBarrier(3);
		for (int i = 0; i < 3; i++) {
			Runnable r = new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(new Random().nextInt(5) * 1000);
						// 有时打印数据不对，数据没有及时同步
						System.out.println(
								"线程" + Thread.currentThread().getName() + " 到达集合点1，当前已有 " + (cb.getNumberWaiting() + 1)
										+ "已经到达，" + (cb.getNumberWaiting() == 2 ? "都到齐了，继续走啊" : "正在等候"));

						cb.await();

						Thread.sleep(new Random().nextInt(5) * 1000);
						System.out.println(
								"线程" + Thread.currentThread().getName() + " 到达集合点2，当前已有 " + (cb.getNumberWaiting() + 1)
										+ "已经到达，" + (cb.getNumberWaiting() == 2 ? "都到齐了，继续走啊" : "正在等候"));

						cb.await();

						Thread.sleep(new Random().nextInt(5) * 1000);
						System.out.println(
								"线程" + Thread.currentThread().getName() + " 到达集合点3，当前已有 " + (cb.getNumberWaiting() + 1)
										+ "已经到达，" + (cb.getNumberWaiting() == 2 ? "都到齐了，继续走啊" : "正在等候"));
						cb.await();
					} catch (InterruptedException | BrokenBarrierException e) {
						e.printStackTrace();
					}

				}
			};
			service.execute(r);
		}

	}
}
