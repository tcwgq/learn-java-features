package com.tcwgq.learn_juc;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CacheDemo {
	// 使用ConcurrentHashMap代替
	private Map<String, Object> cache = new HashMap<>();
	private ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

	public Object getData(String key) {
		rwl.readLock().lock();
		Object value = cache.get(key);
		if (value == null) {
			rwl.readLock().unlock();
			rwl.writeLock().lock();
			// 双重检查
			if (value == null) {
				value = "hello";// 模拟数据库操作
				cache.put(key, value);
			}
			rwl.readLock().lock();// 锁降级
			rwl.writeLock().unlock();
//			rwl.readLock().lock();
		}
		return value;
	}
}
