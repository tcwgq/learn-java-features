package com.tcwgq.learn_juc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectionModifyExceptionTest {
	public static void main(String[] args) {
		List<User> users =  new ArrayList<>(); 
				/*new CopyOnWriteArrayList<>();*/
		users.add(new User("zhangSan", 123));
		users.add(new User("liSi", 1234));
		users.add(new User("wangWu", 12345));
		Iterator<User> iterator = users.iterator();
		while (iterator.hasNext()) {
			User user = iterator.next();
			if (user.name.equals("liSi")) {
				// 删除liSi不会出现问题
				users.remove(user);
			} else {
				System.out.println(user);
			}
		}
	}
}
