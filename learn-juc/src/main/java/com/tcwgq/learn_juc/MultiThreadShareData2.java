package com.tcwgq.learn_juc;

public class MultiThreadShareData2 {
	private int j = 0;

	public static void main(String[] args) {
		MultiThreadShareData2 data = new MultiThreadShareData2();
		Runnable inc = data.new Inc();
		Runnable dec = data.new Dec();
		for (int i = 0; i < 2; i++) {
			new Thread(inc).start();
			new Thread(dec).start();
		}
	}

	private synchronized void inc() {
		j++;
		System.out.println(Thread.currentThread().getName() + " inc " + j);
	}

	private synchronized void dec() {
		j--;
		System.out.println(Thread.currentThread().getName() + " dec " + j);
	}

	class Inc implements Runnable {

		@Override
		public void run() {
			for (int i = 0; i < 100; i++) {
				inc();
			}
		}

	}

	class Dec implements Runnable {

		@Override
		public void run() {
			for (int i = 0; i < 100; i++) {
				dec();
			}
		}

	}

}