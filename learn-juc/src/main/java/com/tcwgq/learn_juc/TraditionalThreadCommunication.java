package com.tcwgq.learn_juc;

public class TraditionalThreadCommunication {
	public static void main(String[] args) {
		final Business bu = new Business();
		new Thread(new Runnable() {
			public void run() {
				for (int i = 1; i <= 50; i++) {
					bu.sub(i);
				}
			}
		}).start();

		for (int i = 1; i <= 50; i++) {
			bu.main(i);
		}
	}
}

class Business {
	private boolean shouldSub = true;

	public synchronized void sub(int i) {
		while (!shouldSub) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int j = 1; j <= 10; j++) {
			System.out.println("sub thread sequece of " + j + ", loop of " + i);
		}
		shouldSub = false;
		this.notify();
	}

	public synchronized void main(int i) {
		while (shouldSub) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		for (int j = 1; j <= 100; j++) {
			System.out.println("main thread sequece of " + j + ", loop of " + i);
		}
		shouldSub = true;
		this.notify();
	}
}
