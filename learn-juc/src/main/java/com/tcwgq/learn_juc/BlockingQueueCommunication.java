package com.tcwgq.learn_juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueCommunication {
	public static void main(String[] args) {
		final Business bu = new Business();
		new Thread(new Runnable() {
			public void run() {
				for (int i = 1; i <= 10; i++) {
					bu.sub(i);
				}
			}
		}).start();

		for (int i = 1; i <= 10; i++) {
			bu.main(i);
		}
	}

	static class Business {
		BlockingQueue<Integer> queue1 = new ArrayBlockingQueue<>(1);
		BlockingQueue<Integer> queue2 = new ArrayBlockingQueue<>(1);

		{
			try {
				System.out.println("init");
				queue2.put(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void sub(int i) {
			try {
				System.out.println("sub begin...");
				queue1.put(1);
				for (int j = 1; j <= 10; j++) {
					System.out.println("sub thread sequece of " + j + ", loop of " + i);
				}
				queue2.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void main(int i) {
			try {
				System.out.println("main begin...");
				queue2.put(1);
				for (int j = 1; j <= 100; j++) {
					System.out.println("main thread sequece of " + j + ", loop of " + i);
				}
				queue1.take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
