package com.tcwgq.learn_juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadPoolTest {
	public static void main(String[] args) {
		ExecutorService threadPool = Executors.newFixedThreadPool(3);
		for (int i = 1; i <= 10; i++) {
			final int task = i;
			threadPool.execute(new Runnable() {
				@Override
				public void run() {
					for (int j = 1; j <= 10; j++) {
						try {
							Thread.sleep(20);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						System.out.println(Thread.currentThread().getName() + " is loop of " + j + " for task " + task);
					}
				}
			});
		}
		System.out.println("end");
		// threadPool.shutdown();
		// threadPool.shutdownNow();
		ExecutorService threadPool1 = Executors.newCachedThreadPool();
		ExecutorService threadPool2 = Executors.newSingleThreadExecutor();
		ScheduledExecutorService threadPool3 = Executors.newSingleThreadScheduledExecutor();
		threadPool3.schedule(new Runnable() {
			@Override
			public void run() {
				System.out.println("bombing!");
			}
		}, 2, TimeUnit.SECONDS);
		threadPool3.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.println("biu!");
			}
		}, 2, 4, TimeUnit.SECONDS);
		System.out.println("end end");
	}
}
