package com.tcwgq.learn_juc;

import java.util.Random;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteLockTest {
	public static void main(String[] args) {
		Queue queue = new Queue();
		for (int i = 0; i < 3; i++) {
			// 读
			new Thread() {
				public void run() {
					while (true) {
						queue.get();
					}
				};
			}.start();
			// 写
			new Thread() {
				public void run() {
					while (true) {
						queue.put(new Random().nextInt(100));
					}
				};
			}.start();
		}
	}
}

class Queue {
	private int data;
	private ReadWriteLock rwl = new ReentrantReadWriteLock();

	public void get() {
		rwl.readLock().lock();
		try {
			System.out.println(Thread.currentThread().getName() + " is ready to get data!");
			Thread.sleep(500);
			System.out.println(Thread.currentThread().getName() + " has gotten data: " + data);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			rwl.readLock().unlock();
		}
	}

	public void put(int data) {
		rwl.writeLock().lock();
		try {
			System.out.println(Thread.currentThread().getName() + " is ready to write data!");
			Thread.sleep(500);
			this.data = data;
			System.out.println(Thread.currentThread().getName() + " has written data: " + data);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			rwl.writeLock().unlock();
		}
	}
}
