package com.tcwgq.learn_juc;

public class MultiThreadShareData {
	private ShareData data1 = new ShareData();
	
	public static void main(String[] args) {
		ShareData data = new ShareData();
		new Thread(new Runnable() {
			@Override
			public void run() {
				data.increment();
			}
		}).start();
		new Thread(new Runnable() {
			@Override
			public void run() {
				data.decrement();
			}
		}).start();
	}

}

class ShareData /* implements Runnable */ {
	private int j = 0;
	private int count = 100;

	/*@Override
	public void run() {
		while (true) {
			count--;
		}
	}
*/
	public synchronized void increment() {
		j++;
	}

	public synchronized void decrement() {
		j--;
	}

}

class ShareData1 implements Runnable{
	private ShareData data;
	
	public ShareData1(ShareData data) {
		super();
		this.data = data;
	}

	@Override
	public void run() {
		while(true) {
			data.decrement();
		}
	}
	
}
