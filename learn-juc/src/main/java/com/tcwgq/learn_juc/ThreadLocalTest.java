package com.tcwgq.learn_juc;

import java.util.Random;

public class ThreadLocalTest {
	private static ThreadLocal<Integer> x = new ThreadLocal<Integer>();
//	private static ThreadLocal<MyThreadScopeData> myThreadLocal = new ThreadLocal<MyThreadScopeData>();

	public static void main(String[] args) {
		for (int i = 0; i < 2; i++) {
			new Thread(new Runnable() {
				public void run() {
					int data = new Random().nextInt(11);
					x.set(data);
					System.out.println(Thread.currentThread().getName() + " has put data of " + data);
//					MyThreadScopeData myData = new MyThreadScopeData();
//					myData.setName("name" + data);
//					myData.setAge(data);
//					myThreadLocal.set(myData);
					MyThreadScopeData.getThreadInstance().setName("name"+data);
					MyThreadScopeData.getThreadInstance().setAge(data);
					new A().get();
					new B().get();
				}
			}).start();
		}
	}

	static class A {
		public void get() {
			int data = x.get();
			System.out.println("A from " + Thread.currentThread().getName() + " get data: " + data);
//			System.out.println("A " + myThreadLocal.get());
			System.out.println("A "+MyThreadScopeData.getThreadInstance());
		}
	}

	static class B {
		public void get() {
			int data = x.get();
			System.out.println("B from " + Thread.currentThread().getName() + " get data: " + data);
//			System.out.println("B " + myThreadLocal.get());
			System.out.println("B "+MyThreadScopeData.getThreadInstance());
		}
	}
}

class MyThreadScopeData {
//	private static volatile MyThreadScopeData s = null;

	private static ThreadLocal<MyThreadScopeData> threadLocalMap = new ThreadLocal<MyThreadScopeData>();

	private MyThreadScopeData() {

	}

	public static /*synchronized*/ MyThreadScopeData getThreadInstance() {
		MyThreadScopeData threadScopeData = threadLocalMap.get();
		if (threadScopeData == null) {
			threadScopeData = new MyThreadScopeData();
			threadLocalMap.set(threadScopeData);
		}
		return threadScopeData;
	}

	private String name;
	private Integer age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "MyThreadScopeData [name=" + name + ", age=" + age + "]";
	}

}