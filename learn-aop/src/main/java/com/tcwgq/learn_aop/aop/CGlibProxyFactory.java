package com.tcwgq.learn_aop.aop;

import com.tcwgq.learn_aop.service.impl.PersonServiceImpl;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CGlibProxyFactory implements MethodInterceptor {
    private Object targetObject;

    public Object createProxyInstance(Object targetObjext) {
        this.targetObject = targetObjext;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(this.targetObject.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        PersonServiceImpl bean = (PersonServiceImpl) this.targetObject;
        Object result = null;
        if (bean.getUsername() != null) {
            try {
                // 前置通知
                result = methodProxy.invoke(targetObject, args);
                // 后置通知
            } catch (Exception e) {
                // 例外通知
            } finally {
                // 最终通知
            }
        }
        return result;
    }
}
