package com.tcwgq.learn_aop.aop;

import com.tcwgq.learn_aop.service.impl.PersonServiceImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JDKProxyFactory implements InvocationHandler {
    private Object targetObject;

    public Object createProxyInstance(Object targetObjext) {
        this.targetObject = targetObjext;
        return Proxy.newProxyInstance(this.targetObject.getClass().getClassLoader(),
                this.targetObject.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        PersonServiceImpl bean = (PersonServiceImpl) this.targetObject;
        Object result = null;
        if (bean.getUsername() != null) {
            result = method.invoke(targetObject, args);
        }
        return result;
    }
}
