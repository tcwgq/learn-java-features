package com.tcwgq.learn_aop.test;

import com.tcwgq.learn_aop.aop.CGlibProxyFactory;
import com.tcwgq.learn_aop.service.impl.PersonServiceImpl;

public class AOPTest1 {
    public static void main(String[] args) {
        CGlibProxyFactory factory = new CGlibProxyFactory();
        PersonServiceImpl service = (PersonServiceImpl) factory.createProxyInstance(new PersonServiceImpl("Hello"));
        service.save("world");
    }
}
