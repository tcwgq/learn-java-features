package com.tcwgq.learn_aop.test;

import com.tcwgq.learn_aop.aop.JDKProxyFactory;
import com.tcwgq.learn_aop.service.PersonService;
import com.tcwgq.learn_aop.service.impl.PersonServiceImpl;

public class AOPTest {
    public static void main(String[] args) {
        JDKProxyFactory factory = new JDKProxyFactory();
        PersonService service = (PersonService) factory.createProxyInstance(new PersonServiceImpl("Hello"));
        service.save("world");
    }
}
