package com.tcwgq.learn_aop.service.impl;

public class PersonServiceImpl {
    private String username;

    public PersonServiceImpl() {
    }

    public PersonServiceImpl(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void save(String name) {
        System.out.println("我是save方法");
    }

    public void update(String name, Integer id) {
        System.out.println("我是update方法");
    }

    public String getPersonName(Integer id) {
        System.out.println("我是getPersonName方法");
        return "Hello world";
    }

}
