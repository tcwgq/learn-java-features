package com.tcwgq.learn_aop.service;

public interface PersonService {
    void save(String name);

    void update(String name, Integer id);

    String getPersonName(Integer id);
}
